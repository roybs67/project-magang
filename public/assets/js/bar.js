window.onload = function () {
	
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        axisX: {
            interval: 1
        },
        axisY: {
            includeZero: true,
            scaleBreaks: {
                type: "straight",
                customBreaks: [{
                    startValue: 100,
                    endValue: 120
                    }
            ]}
        },
        data: [{
            type: "bar",
            toolTipContent: "<img src=\"https://canvasjs.com/wp-content/uploads/images/gallery/javascript-column-bar-charts/\"{url}\"\" style=\"width:40px; height:20px;\"> <b>{label}</b><br>Progress: {y}%",
            dataPoints: [
                { label: "PT Pertiwi", y: 25, gdp: 5.3, url: "russia.png" },
                { label: "PT Setya", y: 50, gdp: 2.5, url: "india.png" },
                { label: "PT Agung", y: 40, gdp: 1.9, url: "uk.png" },
                { label: "PT Indah", y: 50, gdp: 1.0, url: "japan.png" },
                { label: "PT Pratama", y: 70, gdp: 1.2, url: "germany.png" },
                { label: "PT SM", y: 75, gdp: 2.7, url: "skorea.png" },
                { label: "PT Blockberry", y: 93, gdp: 2.0, url: "australia.png" },
                { label: "PT XYZ Jaya", y: 100, gdp: 1.3, url: "brazil.png"},
                { label: "PT ABCDEF", y: 100, gdp: 5.7, url: "uae.png" },
    	        { label: "PT Indo Raja", y: 75, gdp: 5.8, url: "israel.png" },
            ]
        }]
    });
    chart.render();
    
    }

