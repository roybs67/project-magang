window.onload = function () {
	
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
       
        axisX: {
            interval: 1
        },
        axisY: {
            includeZero: true,
            scaleBreaks: {
                type: "straight",
                customBreaks: [{
                    startValue: 100,
                    endValue: 120
                    }
            ]}
        },
        data: [{
            type: "bar",
            toolTipContent: "<img src=\"https://canvasjs.com/wp-content/uploads/images/gallery/javascript-column-bar-charts/\"{url}\"\" style=\"width:40px; height:20px;\"> <b>{label}</b><br>Progress: {y}%",
            dataPoints: [
                { label: "PT SM", y: 75, gdp: 2.7, url: "skorea.png" }
            ]
        }]
    });
    chart.render();
    
    }

