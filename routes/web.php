<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('magang.landing');
});


Route::get('/forgetpassword', function () {
    return view('magang.forgetpassword');
});

// Route::get('/login', function () {
//     return view('magang.login');
// });



// Route::get('/register', function () {
//     return view('magang.register');
// });

// Route::get('/dashboardADMIN', function () {
//     return view('magang.dashboardADMIN');
// });



// Route::get('/newpurchase', function () {
//     return view('magang.newpurchase');
// });



// Route::get('/detailpurchase', function () {
//     return view('magang.detailpurchase');
// });

// Route::get('/editpurchase', function () {
//     return view('magang.editpurchase');
// });

// Route::get('/detailmaterialist', function () {
//     return view('magang.detailmaterialist');
// });

// Route::get('/editmaterialist', function () {  
//     return view('magang.editmaterialist');
// });

// Route::get('/printmaterialist', function () {
//     return view('magang.printmaterialist');
// });

// Route::get('/approval', function () {
//     return view('magang.approval');
// });


// Route::delete('/{id}/editpurchase','App\Http\Controllers\NewPurchaseController@deleteProduct');
// Route::post('/{id}/editpurchase','App\Http\Controllers\NewPurchaseController@tambah')->name('store.editpurchase');


Route::get('/signup','App\Http\Controllers\NewPurchaseController@getSignup')->name('signup');
Route::post('/signup','App\Http\Controllers\NewPurchaseController@postSignup')->name('muka');
Route::get('/login','App\Http\Controllers\NewPurchaseController@getLogin')->name('login');
Route::post('/login','App\Http\Controllers\NewPurchaseController@postLogin')->name('postlogin');
Route::get('/logout','App\Http\Controllers\NewPurchaseController@logout')->name('logout');
Route::get('/forgot_password', 'App\Http\Controllers\ForgotPassword@forgot');
Route::post('/forgot_password', 'App\Http\Controllers\ForgotPassword@password');
Route::get('/reset_password/{email}/{code}', 'App\Http\Controllers\ForgotPassword@reset');
Route::post('/reset_password/{email}/{code}', 'App\Http\Controllers\ForgotPassword@resetPassword');


Route::get('status/{id}', 'App\Http\Controllers\NewPurchaseController@status')->name('status');

Route::group(['middleware' => ['auth', 'ceklevel:Admin,Super Admin']], function(){
    Route::get('/approval', 'App\Http\Controllers\NewPurchaseController@regis')->name('approval');
    Route::get('/editcustomer/{id}','App\Http\Controllers\NewPurchaseController@editCustomer');
    Route::post('/editcustomer/{id}','App\Http\Controllers\NewPurchaseController@addProject')->name('addProject');
    Route::get('/editproject/{id}','App\Http\Controllers\NewPurchaseController@editProject')->name('editProject');
    Route::post('/editproject/{id}','App\Http\Controllers\NewPurchaseController@editUser')->name('editUser');
    Route::get('/deleteproject/{id}','App\Http\Controllers\NewPurchaseController@deleteProject');
    Route::get('/newpurchase', 'App\Http\Controllers\NewPurchaseController@index')->name('newpurchase');
    Route::post('/newpurchase', 'App\Http\Controllers\NewPurchaseController@store')->name('store.newpurchase');

    Route::get('/dashboardADMIN', 'App\Http\Controllers\NewPurchaseController@detail')->name('detail.purchase');
    Route::get('/detailpurchase/{id}', 'App\Http\Controllers\NewPurchaseController@detailPurchase')->name('detail');
    Route::get('/{id}/delete','App\Http\Controllers\NewPurchaseController@delete');
    Route::get('/{id}/editpurchase','App\Http\Controllers\NewPurchaseController@edit');
    Route::post('/{id}/editpurchase','App\Http\Controllers\NewPurchaseController@update');
    Route::get('/{id}/deletep','App\Http\Controllers\NewPurchaseController@deletePurchase');

    Route::get('/materialis', 'App\Http\Controllers\NewPurchaseController@dashboardMateri')->name('dashboard.materi');
    Route::get('/detailmaterialist/{id}', 'App\Http\Controllers\NewPurchaseController@detailMaterialist')->name('detailM');
    Route::get('/printmaterialist/{id}', 'App\Http\Controllers\NewPurchaseController@printMaterialist')->name('printM');

    Route::get('/{id}/editmaterialist','App\Http\Controllers\NewPurchaseController@editMaterialist');
    Route::post('/{id}/editmaterialist','App\Http\Controllers\NewPurchaseController@updateMaterialist');
    Route::get('/{id}/deletematerialist','App\Http\Controllers\NewPurchaseController@deleteMaterialist');
    Route::post('/materialimport','App\Http\Controllers\NewPurchaseController@materialimport')->name('materialimport');
});

Route::group(['middleware' => ['auth', 'ceklevel:Customer']], function(){
    Route::get('/dashboardCUSTOMER','App\Http\Controllers\NewPurchaseController@dashboardCustomer')->name('dashboardCustomer');
});

Route::group(['middleware' => ['auth', 'ceklevel:Manager']], function(){
    Route::get('/dashboardManager','App\Http\Controllers\NewPurchaseController@dashboardManager')->name('dashboardManager');
});

Route::get('/user/activation/{id}', 'App\Http\Controllers\NewPurchaseController@userActivation');