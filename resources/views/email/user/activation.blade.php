@component('mail::message')
# Aktivasi Pengguna

Akun pengguna baru telah ditambah, dengan data sebagai berikut

@component('mail::panel')
<style>
  .c-table td, .c-table th {
  text-align: left;
}
</style>
<table class="c-table">
  <tr>
    <th>Username</th>
    <td>: {{$user->username}}</td>
  </tr>
  <tr>
    <th>Company</th>
    <td>: {{$user->company}}</td>
  </tr>
  <tr>
    <th>Email</th>  
    <td>: {{$user->email}}</td>
  </tr>
</table>
@endcomponent

Silahkan lakukan aktivasi pada akun ini.

@component('mail::button', ['url' => url('/user/activation/'.$user->id.'?username='.$admin->username.'&password='.$admin->password)])
Aktivasi Akun
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
