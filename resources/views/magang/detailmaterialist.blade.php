<!DOCTYPE html>
<html>
<head>
  <title>Detail Materialist</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSEdit.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">
  
  
</head>
<body class="bg-body">
  <header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="{{asset('Logo CBA 50.png')}}" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('detail.purchase')}}"><i
                  class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i
                  class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('dashboard.materi')}}"><i
                  class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
              {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>
  
  <!-- content -->
  <div class="bg-white m-content pb-5"> 
    <div class="row mx-5">
      <!-- head content  -->
      <div class="col-12 pt-3">
        <div class="d-flex d-md-block flex-column-reverse">
          <button
            class="float-none float-md-end btn btn-sm border border-2 border-color rounded d-flex justify-content-center justify-content-md-between align-items-center gap-2">
            <i class="fal fa-chevron-left icon-color"></i><a href="{{route('dashboard.materi')}}" style="color:black;text-decoration:none">Back</a>
          </button>
          <h6 class="f-24 f-blue">{{$purchase->transaction_no}}<i class="far fa-file-alt f-blue ms-4"></i></h6>
        </div>
      </div>
      <div class="hr">
        <hr>
      </div>
      <span class="f-18 f-blue">Purchase Information</span>
      <!-- end head content  -->
      <!-- isi form  -->
      <form>
          <div class="row">
            <!-- row 1 -->
                <div class="col-md-5 mt-2">
                  <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor</label><br>
                  <span class="f-13 f-blue ">{{ $purchase->vendor}}</span>
                </div>
                <div class="col-md-5 mt-2">
                  <label for="disabledTextInput1" class="form-label f-14 fw-bold">Transaction Date:</label><br>
                  <span class="f-13 f-blue">{{ $purchase->transaction_date}}</span>
                </div>
                <div class="col-md-2 mt-2">
                  <label for="disabledTextInput1" class="form-label f-14 ms-auto fw-bold">Tower Type:</label><br>

                  <span class="f-13 f-blue">{{ $purchase->materialist?$purchase->materialist->tower_type:""}}</span>
                </div>
          </div>

          <div class="row">
            <!-- row 1 -->
                <div class="col-md-5 mt-3">
                  <label for="disabledSelect1" class="form-label f-14 text fw-bold">Email</label><br>
                  <span class="f-13 f-blue ">{{ $purchase->email}}</span>
                </div>
                <div class="col-md-5 mt-3">
                  <label for="disabledTextInput1" class="form-label f-14 fw-bold">Transaction No:</label><br>
                  <span class="f-13 f-blue">{{ $purchase->transaction_no}}</span>
                </div>
                <div class="col-md-2 mt-3">
                  <label for="disabledTextInput1" class="form-label f-14 ms-auto fw-bold">Packing List No.:</label><br>
                  <span class="f-13 f-blue">{{ $purchase->materialist?$purchase->materialist->pack_list_no:""}}</span>
                </div>
          </div>

          <div class="row">
            <!-- row 1 -->
            <div class="col-md-5 mt-3">
              <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor Address</label><br>
              <span class="f-13 f-blue ">{{ $purchase->vendor_adress}}</span>
            </div>
            <div class="col-md-5 mt-3">
              <label for="disabledTextInput1" class="form-label f-14 fw-bold">Vendor Ref.No.</label><br>
              <span class="f-13 f-blue">{{ $purchase->vendor_ref_no}}</span>
            </div>
            <div class="col-md-2 mt-3">
              <label for="disabledTextInput1" class="form-label f-14 ms-auto fw-bold">Number of Revision:</label><br>
              <span class="f-13 f-blue">{{ $purchase->materialist?$purchase->materialist->num_rev:""}}</span>
            </div>
          </div>

          <br><br>
          <span class=" f-18 f-blue">Product Data</span>
          <div class="table-responsive">
            <table class="table table-borderless mt-2">
              <thead class="header-color">
                <tr class="tab custom-rounded">
                  <th class="f-14 f-blue" scope="col">Marking No.</th>
                  <th class="f-14 f-blue" scope="col">Section</th>
                  <th class="f-14 f-blue text-right" scope="col" colspan="2">Profile</th>
                  <th class="f-14 f-blue text-right" scope="col" colspan="2" >Length(in mm)</th>
                  <th class="f-14 f-blue text-right" scope="col" colspan="">Qty(in pcs)</th>
                  <th class="f-14 f-blue text-right" scope="col" colspan="">Qty Units</th>
                  <th class="f-14 f-blue  text-right" scope="col">Total Qty</th>
                  <th class="f-14 f-blue text-right" scope="col">Done</th>
                </tr>
              </thead>
              <tbody>
              <!-- @if(
                $purchase ->materialistpro
              ) -->
              @foreach ($purchase ->materialistpro as $cust)
                <tr>
                  <td class="f-13 f-blue" scope="col">{{ $cust->mark_no}}</td>
                  <td class="f-13 f-blue">{{ $cust->section}}</td>
                  <td class="f-13 f-blue">{{ $cust->profile1}}</td>
                  <td class="f-13 f-blue">{{ $cust->profile2}}</td>
                  <td class="f-13 f-blue">{{ $cust->profile3}}</td>
                  <td class="f-13 f-blue text-right">{{ $cust->length}}</td>
                  <td class="f-13 f-blue text-right">{{ $cust->qty}}</td>
                  <td class="f-13 f-blue text-right">{{ $cust->qty_units}}</td>
                  <td class="f-13 f-blue text-right">{{ $cust->tot_qty}}</td>
                  <td class="f-13 f-blue text-right">{{ $cust->done}}</td>
                </tr>
                @endforeach
                <!-- @endif -->
                <!-- <tr>
                  <td class="f-13 f-blue" scope="col">CLT-A2-01</td>
                  <td class="f-13 f-blue">STUB&CLEAT</td>
                  <td class="f-13 f-blue">L</td>
                  <td class="f-13 f-blue">120</td>
                  <td class="f-13 f-blue">8</td>
                  <td class="f-13 f-blue text-right">250</td>
                  <td class="f-13 f-blue text-right">12</td>
                  <td class="f-13 f-blue text-right">2</td>
                  <td class="f-13 f-blue text-right">24</td>
                  <td class="f-13 f-blue text-right">16</td>
                </tr> -->
              </tbody>
            </table>
          </div>

          <div class="row">
            <div class="col-md-7">
              <label for="disabledSelect" class=" mt-2 f-14 fw-bold">Acknowledged:</label><br>
              <span class="f-13 f-blue">{{ $purchase->materialist?$purchase->materialist->acknowledged:""}}</span><br>
              <label for="disabledSelect" class=" mt-3 f-14 fw-bold">Warehouse:</label><br>
              <span class="f-13 f-blue">{{ $purchase->materialist?$purchase->materialist->warehouse:""}}</span><br>
              <label for="disabledSelect" class=" mt-3 f-14 fw-bold">Recipient:</label><br>
              <span class="f-13 f-blue">{{ $purchase->materialist?$purchase->materialist->recipient:""}}</span><br>
            </div>

            <div class="col-md-5 mt-2 text-end">
              <div class="row justify-content-end align-items-center">
                <label for="disabledTextInput" class="col-9 f-14 f-blue text-start">Total Qty</label><br>
                <label for="disabledTextInput" class="col-3 f-14 f-blue text-end">{{ $purchase->materialist?$purchase->materialist->total_qty:""}}</label><br>

                <label for="disabledSelect" class="col-9 form-label mt-1 f-14 f-blue text-start">Total Done</label><br>
                <span class="col-3 f-14 f-blue text-end">{{ $purchase->materialist?$purchase->materialist->total_done:""}}</span><br>

                <label for="disabledSelect"
                  class="col-9 form-label  mt-1 f-18 f-blue text-start"><b>Progress</b></label>
                <span class="col-3 f-18 f-blue text-end"><b>{{ $purchase->materialist?$purchase->materialist->progress:""}}%</b>
              </div>
              <div class="row gap-2 gap-md-1 gap-lg-5 mt-5 justify-content-end">
                <button type="button" class="col-sm-auto btn btn-sm btn-outline-primary"><a href="{{url('/printmaterialist/'.$purchase->id)}}" target="_blank" style="color:black;text-decoration:none">Print & Preview</a></button> 
                <div class="col-sm-auto">
                  <div class="row gap-2 gap-md-1 gap-lg-2">
                    <button type="button" class="col-sm-auto btn btn-sm btn-outline-danger"><a href="/{{$purchase->id}}/deletematerialist" onclick="return confirm('Yakin mau dihapus ?')" style="color:black;text-decoration:none">Delete</a></button>
                    <button type="button" class="col-sm-auto btn btn-sm btn-success"><a href="/{{$purchase->id}}/editmaterialist" style="color:white;text-decoration:none" >Edit</a></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </form>
    </div>
  </div>
  <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3" ></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>    

</body>
</html>    
    