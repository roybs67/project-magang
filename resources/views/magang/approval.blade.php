
<!DOCTYPE html>
<html>
<head>
  <title>Detail Purchase</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSEdit.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">
  
  
</head>
<body class="bg-body">
  <header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="{{asset('Logo CBA 50.png')}}"alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('detail.purchase')}}"><i class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('dashboard.materi')}}"><i class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item active"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul
              class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
                  {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>

    </nav>
  </header>

  <!-- content -->
  <div class="bg-white m-content pb-5"> 
    <div class="row mx-5">
      <!-- head content  -->
      <div class="col-12 pt-3">
        <!-- <div class="d-flex d-md-block flex-column-reverse">
          <button
            class="float-none float-md-end btn btn-sm border border-2 border-color rounded d-flex justify-content-center justify-content-md-between align-items-center gap-2">
            <i class="fal fa-chevron-left icon-color"></i><a href="{{route('detail.purchase')}}" style="color:black;text-decoration:none">Back</a>
          </button>
          <h6 class="f-24 f-blue"><i class="far fa-file-alt f-blue ms-4"></i></h6>
        </div> -->
      </div>
      
      <form>
        <br><br>
        <span class=" f-18 f-blue">User Data</span>
        <div class="table-responsive">
          <table class="table  table-borderless mt-2">
            <thead class="header-color">
              <tr class="tab custom-rounded">
                <th class="f-14 f-blue   " scope="col">Username</th>
                <th class="f-14 f-blue " scope="col">Company</th>
                <th class="f-14 f-blue text-right " scope="col">Email</th>
                <!-- <th class="f-14 f-blue " scope="col">Password</th> -->
                <th class="f-14 f-blue text-right " scope="col">Role</th>
                <th class="f-14 f-blue " scope="col">Status</th>
                <th class="f-14 f-blue text-right  " scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @if (auth()->user()->role=="Super Admin")
                @foreach($users as $user)
                <tr>
                @if ($user->role== 'Manager' OR $user->role== 'Customer' OR $user->role== 'Admin' OR $user->role== 'employee' ) 
                    <td class="f-13 f-blue" scope="col">{{$user->username}}</td>
                    <td class="f-13 f-blue">{{$user->company}}</td>
                    <td class="f-13 f-blue text-right">{{$user->email}}</td>
                    <!-- <td class="f-13 f-blue">{{$user->password}}</td> -->
                    <td class="f-13 f-blue text-right">{{$user->role}}</td>
                    <td class="f-13 f-blue">@if($user->status == 0) Inactive @else Active @endif</td>
                    <td class="f-13 f-blue text-right">
                    
                    @if ($user->status == 1)  
                    <a  class="btn btn-primary btn-sm" href="{{route('status', ['id'=>$user->id]) }}">Inactive</a>
                    @else 
                    <a class="btn btn-primary btn-sm" href="{{route('status', ['id'=>$user->id]) }}">Active</a>
                    @endif
                    @if ($user->role== 'Customer') 
                    <a class="btn btn-primary btn-sm" href="{{url('/editcustomer/'.$user->id)}}" role="button">Add</a>
                    <a class="btn btn-primary btn-sm" href="{{url('/editproject/'.$user->id)}}" role="button">Edit</a>
                    @elseif ($user->role== 'Admin' OR $user->role== 'Manager' OR  $user->role== 'employee')
                    <a class="btn btn-primary btn-sm" href="{{url('/editproject/'.$user->id)}}" role="button">Edit</a>
                    @endif
                    </td>
                  @endif
                </tr>
                @endforeach
              
              @elseif (auth()->user()->role=="Admin")
              @foreach($users as $user)
                <tr>
                @if ($user->role== 'Manager' OR $user->role== 'Customer' OR $user->role== 'employee' )
                    <td class="f-13 f-blue" scope="col">{{$user->username}}</td>
                    <td class="f-13 f-blue">{{$user->company}}</td>
                    <td class="f-13 f-blue text-right">{{$user->email}}</td>
                    <!-- <td class="f-13 f-blue">{{$user->password}}</td> -->
                    <td class="f-13 f-blue text-right">{{$user->role}}</td>
                    <td class="f-13 f-blue">@if($user->status == 0) Inactive @else Active @endif</td>
                    <td class="f-13 f-blue text-right">
                    @if ($user->status == 1)  
                    <a  class="btn btn-primary btn-sm" href="{{route('status', ['id'=>$user->id]) }}">Inactive</a>
                    @else 
                    <a class="btn btn-primary btn-sm" href="{{url('/editcustomer/'.$user->id)}}" role="button">Active</a>
                    @endif
                    @if ($user->role== 'Customer') 
                    <a class="btn btn-primary btn-sm" href="{{url('/editcustomer/'.$user->id)}}" role="button">Add</a>
                    <a class="btn btn-primary btn-sm" href="{{url('/editproject/'.$user->id)}}" role="button">Edit</a>
                    @elseif ($user->role== 'Manager' OR $user->role== 'employee' )
                    <a class="btn btn-primary btn-sm" href="{{url('/editproject/'.$user->id)}}" role="button">Edit</a>
                    @endif
                    </td>
                  @endif
                </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </form>
    </div>
  </div>
  <br>
  <br>
  <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3" ></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div>
  <!-- <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3"></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div> -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
  