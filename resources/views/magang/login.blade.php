
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Landing</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style.css') !!}">
    <style>
      body {
        background-image:url("Variant5.png")
      }
    </style>
</head>
<body style="height: 100vh;">
    <header class="fixed-top">
        <!-- head -->
        <nav class="navbar navbar-expand-lg navbar-dark color-nav">
            <div class="container-fluid mx-4">
                <a class="navbar-brand" href="#">
                    <img src="{{asset('Logo CBA 50.png')}}" alt="" width="74" height="44"
                        class="d-inline-block align-text-top">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span></button>

                <div class="collapse navbar-collapse ms-3" id="navbarText">
                    <ul class="navbar-nav gap-2 gap-lg-0">
                        <li class="nav-item rounded py-0">
                            <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item active"
                                aria-current="page" href="#"><i class="fal fa-sign-in me-2"></i>Log in</a>
                        </li>
                        <li class="nav-item rounded ms-lg-4">
                            <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                                aria-current="page" href="{{route('signup')}}"><i class="fal fa-user-plus me-2"></i></i>Sign up</a>
                        </li>
                    </ul>
                    <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
                        <ul
                            class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
                            <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-14">
                                    Welcome Visitor! <br> <span class="f-12">Please log in or sign up</span>
                            </li>
                            <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Pro No.png')}}" alt=""
                                    width="50" height="50">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <main class="container px-md-5 main d-flex flex-column justify-content-center align-items-center">
    <div class="login-card">
        <p class="t-login">Login</p>
        <form class="p-form" name="myForm" onsubmit="return validateForm()" method="POST" action="{{ route('postlogin')}}">
        {{ csrf_field() }} 
            <div class="container">
                <label for="uname">Username</label>
                <input type="text" placeholder="Enter Username" name="username" required>

                <label for="psw">Password</label>
                <input type="password" placeholder="Enter Password" name="password" id="password" required>
                <span class="eyesss"><i class="fa fa-eye" aria-hidden="true" id="eye" onclick="toggle()"></i> </span>
                <span class="psw"><a href="{{url ('/forgot_password')}}">Forgot Password</span>
                <button type="submit"  class="button"> Log In</button>
                @if(Session::has('message'))
                <button class="buttons">{!! session('message')!!}</button>
                @endif
                <!-- @if(Session::has('message'))
                 <div class="alert alert-warning alert-dismissible fade show" role="alert" >{!! session('message')!!}
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" style="text-decoration:none"></button>
                </div>
                 @endif -->
                 
                 
            </div>
        </form>
    </div>
    </main>
    <div class="footer <!--fixed-bottom--> d-flex flex-column justify-content-center">
        <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
            <div class="float-md-end float-none text-center text-white list">
                <i class="fal fa-envelope f me-3"></i>
                <i class="fal fa-phone-alt f me-3"></i>
                <i class="fal fa-map-marker-alt f "></i>
            </div>
            <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
                Abadi All Rights Reserved.
            </span>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>  
    <script>
        var state= false;
        function toggle(){
            if(state) {
                document.getElementById("password").setAttribute("type", "password");
                document.getElementById("eye").style.color='#7a797e';
                state=false;
            }
            else{
                document.getElementById("password").setAttribute("type", "text");
                document.getElementById("eye").style.color='#5887ef';
                state=true;
            }
        }
    </script>  
</body>
</html>