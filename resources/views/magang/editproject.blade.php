
<html>
<head>
  <title>Edit Materialist</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSEdit.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">
  

</head>
<body class="bg-body">
  <header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="{{asset('Logo CBA 50.png')}}" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('detail.purchase')}}"><i
                  class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i
                  class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('dashboard.materi')}}"><i
                  class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
              {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <!-- content -->
  <form action="{{route('editUser',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
     {{ csrf_field() }}
  <div class="bg-white m-content pb-5" > 
    <div class="row mx-4 ">
      <!-- head content  -->
      
      <div class="hr">
        <hr>
      </div>
      <span class="f-18 f-blue">User Information</span>
      
      <!-- end head content  -->
      <!-- isi form  -->
      <div class="row">
        <!-- row 1 -->
            <div class="col-md-5 mt-2">
              <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor</label><br>
              <span class="f-13 f-blue ">{{ $user->company}}</span>
            </div>
            <div class="col-md-5 mt-2">
              <label for="disabledTextInput1" class="form-label f-14 fw-bold">Email</label><br>
              <span class="f-13 f-blue">{{ $user->email}}</span>
            </div>
            <div class="col-md-2 mt-2">
              <label for="disabledTextInput1" class="form-label f-14 fw-bold">Role</label><br>
              <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="AA2+3" name="role"  value="{{$user->role}}" >
              <!-- <select id="disabledSelect" class="form-select f-13 f-blue">
                <option>AA2+3</option>
              </select> -->
            </div>
            <table class="table table-borderless mt-3">
              <thead class="header-color">
                <tr class="tab custom-rounded">
                  <th class="f-14 f-blue" scope="col">Project Id</th>
                  <th class="f-14 f-blue" scope="col">Vendor</th>
                  <th class="f-14 f-blue" scope="col">Action</th>
                </tr>
              </thead>
            <tbody>
              @foreach ($user ->project as $cust)
                <tr>
                  <td class="f-13 f-blue" scope="col">{{ $cust->id}}</td>
                  <td class="f-13 f-blue" scope="col">{{ $cust->vendor}}</td>
                  <td>
                  <a class="btn btn-danger btn-sm" href="{{url('/deleteproject/'.$user->id)}}" role="button"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
             @endforeach
              </tbody>
              </table>
      </div>
     
      <!-- end row 1 -->
      <!-- row 2 -->
     
   
     
        <div class="row justify-content-end mt-3">
          <div class="col-sm-auto">
            <div class="row gap-2 gap-md-1 gap-lg-2">
              <button class="col-sm-auto btn btn-sm btn-success">Save</button>
            </div>
          </div>  
        </div>
        <br>
        <br>
        <br>

      </div>
    </div>
  </div>
  </form>
  <br>
  <br>
  <br>
  <!-- Modal -->

  <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3"></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  
</script>
</body>
</html>