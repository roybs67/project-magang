
<!DOCTYPE html>
<html>
<head>
  <title>Detail Purchase</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSEdit.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">
  
  
</head>
<body class="bg-body">
  <header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="{{asset('Logo CBA 50.png')}}"alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('detail.purchase')}}"><i class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('dashboard.materi')}}"><i class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
             <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul
              class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
              {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <!-- content -->
  <div class="bg-white m-content pb-5"> 
    <div class="row mx-5">
      <!-- head content  -->
      <div class="col-12 pt-3">
        <div class="d-flex d-md-block flex-column-reverse">
          <button
            class="float-none float-md-end btn btn-sm border border-2 border-color rounded d-flex justify-content-center justify-content-md-between align-items-center gap-2">
            <i class="fal fa-chevron-left icon-color"></i><a href="{{route('detail.purchase')}}" style="color:black;text-decoration:none">Back</a>
          </button>
          <h6 class="f-24 f-blue">{{$purchase->transaction_no}}<i class="far fa-file-alt f-blue ms-4"></i></h6>
        </div>
      </div>
      <div class="hr">
        <hr>
      </div>
      <span class="f-18 f-blue">Purchase Information</span>
      <!-- end head content  -->
      <!-- isi form  -->
      <form>
        <div class="row">
          <!-- row 1 -->
          <div class="col-md-5 mt-2">
            <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor</label><br>
            <span class="f-13 f-blue ">{{ $purchase->vendor}}</span>
          </div>
          <div class="col-md-5 mt-2">
            <label for="disabledTextInput1" class="form-label f-14 fw-bold">Transaction Date:</label><br>
            <span class="f-13 f-blue">{{ $purchase->transaction_date}}</span>
          </div>
          <div class="col-md-2 mt-2">
            <label for="disabledTextInput1" class="form-label f-14 ms-auto fw-bold">Transaction No:</label>
            <span class="f-13 f-blue">{{ $purchase->transaction_no}}</span>
          </div>
        </div>

        <div class="row">
          <!-- row 1 -->
          <div class="col-md-5 mt-3">
            <label for="disabledSelect1" class="form-label f-14 text fw-bold">Email</label><br>
            <span class="f-13 f-blue ">{{ $purchase->email}}</span>
            
          </div>
          <div class="col-md-5 mt-3">
            <label for="disabledTextInput1" class="form-label f-14 fw-bold">Due Date:</label><br>
            <span class="f-13 f-blue">{{ $purchase->due_date}}</span>
          </div>
          <div class="col-md-2 mt-3">
            <label for="disabledTextInput1" class="form-label f-14 ms-auto fw-bold">Vendor Ref.No.</label><br>
            <span class="f-13 f-blue">{{ $purchase->vendor_ref_no}}</span>
          </div>
        </div>

        <div class="row">
          <!-- row 1 -->
          <div class="col-md-5 mt-3">
            <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor Adress</label><br>
            <span class="f-13 f-blue ">{{ $purchase->vendor_adress}}</span>
          </div>
          <div class="col-md-5 mt-3">
            <label for="disabledTextInput1" class="form-label f-14 fw-bold">Term:</label><br>
            <span class="f-13 f-blue">{{ $purchase->term}}</span>
          </div>
        </div>
        <br><br>
        <span class=" f-18 f-blue">Product Data</span>
        <div class="table-responsive">
          <table class="table  table-borderless mt-2">
            <thead class="header-color">
              <tr class="tab custom-rounded">
                <th class="f-14 f-blue   " scope="col">Product</th>
                <th class="f-14 f-blue " scope="col">Description</th>
                <th class="f-14 f-blue text-right " scope="col">Qty</th>
                <th class="f-14 f-blue " scope="col">Units</th>
                <th class="f-14 f-blue text-right " scope="col">Unit Price (in IDR)</th>
                <th class="f-14 f-blue " scope="col">Tax</th>
                <th class="f-14 f-blue text-right  " scope="col">Amount (in IDR)</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($purchase ->detail as $cust)
              <tr>
                <td class="f-13 f-blue" scope="col">{{$cust->select_product}}</td>
                <td class="f-13 f-blue">{{$cust->desc}}</td>
                <td class="f-13 f-blue text-right">{{$cust->qty}}</td>
                <td class="f-13 f-blue">{{$cust->units}}</td>
                <td class="f-13 f-blue text-right">{{number_format($cust->units_price, 0, '' , '.')}}</td>
                <td class="f-13 f-blue">0{{$cust->subtax}}</td>
                <td class="f-13 f-blue text-right">{{number_format($cust->amount, 0, '' , '.')}}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>

        <div class="row">
          <div class="col-md-7">
            <label for="disabledSelect" class=" mt-2 f-14 fw-bold">Message:</label><br>
            <span class="f-13 f-blue">{{ $purchase->message}}</span><br>
            <label for="disabledSelect" class=" mt-3 f-14 fw-bold">Note:</label><br>
            <span class="f-13 f-blue">{{ $purchase->note}}</span><br>
            <label for="disabledSelect" class=" mt-3 f-14 fw-bold">Attachments:</label><br>
            <button class="btn card p-0 rounded-3" style="max-width: 18rem;">
              <div class="row g-0">
                <div class="col-md-auto p-3 bg-lightgrey d-flex justify-content-center align-items-center">
                  <i class="fas fa-file-pdf text-danger h4 mb-0"></i>
                </div>
                <div class="col-md-auto">
                  <div class="card-body text-start p-2">
                    <p class="card-title f-13"><a href="{{asset('pdf/'. $purchase->attachments)}}" target="_blank" rel="noopener noreferrer" style="color:black;text-decoration:none">{{ $purchase->attachments}}</a></p>
                    <p class="card-subtitle small"><small class="text-muted">5.0MB</small></p>
                  </div>
                </div>
              </div>
            </button>
          </div>

          <div class="col-md-5 mt-3 text-end">
            <div class="row justify-content-end align-items-center">
              <label for="disabledTextInput" class="col-sm-4 f-14 f-blue text-start">Sub total</label><br>
              <label for="disabledTextInput" class="col-sm-8 f-14 f-blue text-end">Rp.{{number_format($purchase->subtotal, 0, '' , '.')}},00</label><br>

              <label for="disabledSelect" class="col-sm-4 form-label mt-1 f-14 f-blue text-start">Tax</label><br>
              <span class="col-sm-8 f-14 f-blue text-end">Rp.{{number_format($purchase->tax, 0, '' , '.')}},00</span><br>

              <label for="disabledSelect" class="col-sm-4 form-label  mt-1 f-18 f-blue text-start"><b>Total</b></label>
              <span class="col-sm-8 f-18 f-blue text-end"><b>Rp.{{number_format($purchase->totalakhir, 0, '' , '.')}},00</b>
            </div>
            <div class="row gap-2 gap-md-1 gap-lg-5 mt-5 justify-content-end">
              <div class="col-sm-auto">
                <div class="row gap-2 gap-md-1 gap-lg-2">
                  <button type="button" class="col-sm-auto btn btn-sm btn-outline-danger"> <a href="/{{$purchase->id}}/deletep" onclick="return confirm('Yakin mau dihapus ?')" style="color:black;text-decoration:none"> Delete</a></button>
                  <button type="button" class="col-sm-auto btn btn-sm btn-success"><a href="/{{$purchase->id}}/editpurchase" style="color:white;text-decoration:none" >Edit</a></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3"></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
  