<html>
<head>
  <title>New Purchase</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/newpurchase.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">

</head>
<body class="bg-body">
  <header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="{{asset('Logo CBA 50.png')}}" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('detail.purchase')}}"><i class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item active" aria-current="page"
                href="{{route('newpurchase')}}"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('dashboard.materi')}}"><i class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
              {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <!-- content -->
  <div class="bg-white m-content pb-5"> 
    <div class="row mx-4 ">
      <!-- head content  -->
      <div class="col-12 pt-3">
        <div class="d-flex d-md-block flex-column-reverse">
          <button
          type="button"
            class="float-none float-md-end btn btn-sm border border-2 border-color rounded d-flex justify-content-center justify-content-md-between align-items-center gap-2">
            <i class="fal fa-chevron-left icon-color"></i> <a href="{{route('detail.purchase')}}" style="color:black;text-decoration:none">Back</a>
          </button>
          <h6 class="f-24 f-blue">New Purchase Order <i class="fas fa-shopping-cart f-blue ms-2"></i></h6>
        </div>
      </div>
      <div class="hr">
        <hr>
      </div>
      <span class="f-18 f-blue">Purchase Information</span>
      
      <!-- end head content  -->
      <!-- isi form  -->
      <form class="pb-5" action="{{ route('store.newpurchase') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
            @if (session('status'))
              <div class="alert alert-success">
                {{ session('status') }}
              </div>
            @endif  
        <div class="row">
            <!-- row 1 -->
            <div class="row">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-7">
                    <label for="disabledSelect" class="form-label mt-2 f-14">Vendor</label>
                      <!-- <select id="disabledSelect" class="form-select  f-13" name="vendor">
                        <option value="PT.Indo Raja">PT.Indo Raja</option>
                        <option value="PT.Mercusuar">PT.Mercusuar</option>
                        <option value="PT.Jelita">PT.Jelita</option>
                      </select> -->
                      <input type="text" id="disabledSelect1"  class="form-control f-13" placeholder="PT...." name="vendor">
                  </div>
                  <div class="col-md-5 mt-2">
                    <label for="disabledTextInput" class="form-label f-14 ">Email</label>
                    <input type="email" id="emailInput" class="form-control f-13 " placeholder="indorajainfo@hotmail.com" name="email">
                  </div> 
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <label for="disabledSelect1" class="form-label mt-2 f-14">Transaction Date</label>
                    <input type="date" id="disabledTextInput1"  class="form-control f-13" placeholder="13/04/21" name="transaction_date"> 
                  </div>
                  <div class="col-md-6 mt-2">
                    <label for="disabledTextInput1" class="form-label f-14 ">Due Date</label>
                    <input type="date" id="disabledTextInput1" class="form-control f-13 " placeholder="07/06/21" name='due_date'>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <label for="vendorAddress" class="form-label f-14 mt-2">Vendor Address</label>
                <textarea class="form-control" type="textarea" id="vendorAddress" rows="4" name="vendor_adress" placeholder="Jalan Sembada Gg. Sederhana No. 1C Medan Sumatera Utara - Indonesia" style="height: 75%;"></textarea>
              </div>
            </div>
            <!-- end row 1 -->
            <!-- row 2 -->
            <div class="row">
              <div class="col-md-5 mt-2">
                <label for="disabledSelect1" class="form-label f-14">Transaction No.</label>
                <input type="text" id="disabledSelect1"  class="form-control f-13" placeholder="Invoice#00003" name="transaction_no"> 
              </div>
              <div class="col-md-5 mt-2">
                <label for="disabledTextInput1" class="form-label f-14 ">Vendor Ref. No.</label>
                <input type="text" id="disabledTextInput1" class="form-control f-13" placeholder="-" name="vendor_ref_no">
              </div>
              <div class="col-md-2 mt-2">
                <label for="disabledTextInput1" class="form-label f-14 ms-auto">Term</label>
                <select type="text" id="disabledTextInput1" class="form-select f-13" placeholder="Net 30" name="term">
                    <option value="10">Net 10</option>
                    <option  value="30">Net 30</option>
                    <option  value="40">Net 40</option>
                </select>
              </div>
            </div>
            <span class=" f-18 f-blue pt-4">Product Data</span>
            <!-- end row 2 -->
            <!-- row 3 -->

            <div class="row" >
              <!-- <div class="col">
                <div class="row"> -->
              <div class="col-2 d-none d-md-block">
                <label for="disabledSelect" class="form-label mt-2 f-14 f-blue">Product</label>
              </div>
              <div class="col-2 mt-2 d-none d-md-block">
                <label for="disabledTextInput" class="form-label f-14 f-blue ">Description</label>
              </div>
              <div class="col-1 mt-2 text-right d-none d-md-block">
                <label for="disabledTextInput" class="form-label f-14 f-blue">Qty</label>
              </div>
              <div class="col-1 mt-2 d-none d-md-block">
                <label for="disabledTextInput" class="form-label f-14 f-blue">units</label>
              </div>
              <div class="col-1 mt-2 d-none d-md-block">
                <label for="disabledTextInput" class="form-label f-14 f-blue">Tax</label>
              </div>
              <div class="col-2 mt-2 text-right d-none d-md-block">
                <label for="disabledTextInput" class="form-label f-14 f-blue">Unit Price (in IDR)</label>
              </div>
              <div class="col-2 mt-2 text-right d-none d-md-block">
                <label for="disabledTextInput5" class="form-label f-14 f-blue ">Amount in IDR</label>
              </div>
              <div class="col-1 ms-auto mt-3 d-none d-md-block">
                <label for="disabledTextInput5" class="form-label f-14 f-blue "></label>
              </div>
              <!-- </div>
              </div> -->
            </div>
            <div class="row" id="pelatihan0">
              <div class="col-md-2">
                <label for="disabledSelect" class="form-label f-14 f-blue d-md-none">Product</label>
                <!-- <select id="disabledSelect" class="form-select f-13 f-blue" name="select_product[0]">
                    <option  value="Produk 1">Produk 1</option>
                    <option value="Produk 2">Produk 2</option>
                    <option value="Produk 3">Produk 3</option>
                    <option value="Produk 4">Produk 4</option>
                </select> -->
                <input type="text" id="disabledTextInput" class="form-control f-13" placeholder="Produk..." name="select_product[0]">
              </div>
              <div class="col-md-2">
                <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Description</label>
                <textarea type="text" rows="1" id="disabledTextInput" class="form-control f-13 "
                  placeholder="-" name="desc[0]"></textarea>
              </div>
              <div class="col-md-1">
                <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Qty</label>
                <input type="text" id="disabledTextInput" class="form-control f-13" placeholder="..349" name="qty[0]">
              </div>
              <div class="col-md-1">
                <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Units</label>
                <!-- <select type="text" id="disabledTextInput" class="form-select f-13 " placeholder="Kg" name="units[0]">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select> -->
                <input type="text" id="disabledTextInput" class="form-control f-13 text-right" placeholder="Ton" name="units[0]">
                </div>
      
              <div class="col-md-1">
                <label for="disabledTextInput" class="form-label m-0 f-blue d-md-none">Tax</label>
                <select type="text" id="disabledTextInput" class="form-select f-13" placeholder="PPN" name="subtax[0]">
                    <option  value=".00" >Free Tax</option>
                    <option selected value=".1">10%</option>
                    <option selected value=".2">20%</option>
                    <option selected value=".3">30%</option>
                </select>
              </div>
              <div class="col-md-2">
                <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Unit Price (in IDR)</label>
                <input type="text" id="disabledTextInput" class="form-control f-13 text-right"
                  placeholder="23.000,00" name="units_price[0]">
              </div>
              <div class="col-md-2">
                <label for="disabledTextInput5" class="form-label f-14 f-blue d-md-none">Amount in IDR</label>
                <input type="text" id="disabledTextInput5" class="form-control f-13 text-right"
                  placeholder="445.027.000.00" name="amount[0]">
              </div>
              <div class="col-md-1 mt-3 mt-md-0">
                <button type="button" class="btn btn-sm btn-outline-danger rounded remove" id="hapus"><i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="moredata" id="form_dinamis"></div>
            
            <!-- <div class="row">
              <div class="col-md-2 mt-2">
                <button  type="button" class=" addmoredata btn btn-primary rounded fs-12 "  id="tambah"><i class="fal fa-file-plus me-2 ms-2"> </i>  <span class="me-3">Add More Data</span>  </button>
              </div>
            </div> -->
            <div class="row">
              <div class="col-md-12 mt-2">
                <button  type="button" class="addmoredata btn btn-primary rounded fs-12 "  id="tambah"><i class="fal fa-file-plus me-2 ms-2"> </i> <span
                    class="me-3">Add More Data</span> </button>
              </div>
            </div>
            <div class="row fs-12 ms-auto fl-right " width="100%">
                <span class="col-md-8"></span>
                <span class="col-md-1 txt-col p-0" >Sub Total</span>
                <span class="col-md-3 text-right pe-5" id="subtotal">Rp. 00,00</span>
                <input type="text" name="subtotal" value="30000" hidden/>
              
            </div>
            <div class="row fs-12 ms-auto fl-right " width="100%">
              <span class="col-md-8"></span>
              <span class="col-md-1 txt-col p-0">Tax</span>
              <span class="col-md-3 text-right pe-5" id="taxppn">Rp. 00,00</span>
              <input type="text" name="tax" value="30000" hidden/>
            
            </div>
            <div class="row fs-12 ms-auto fl-right " width="100%">
              <span class="col-md-8"></span>
              <span class="col-md-1 txt-col p-0"><strong>Total</strong></span>
              <span class="col-md-3 text-right pe-5" id="totalakhir"><strong>Rp. 00,00</strong></span>
              <input type="text" name="totalakhir" value="30000" hidden/>
            </div>
            <div class="row mt-2">
              <div class="col-md-4">
                <label for="Message" class="form-label">Message</label>
                <textarea id="Message" class="form-control" name="message" >DP 30%</textarea>
              </div>
              <div class="col-md-4">
                <label for="Message" class="form-label">Note</label>
                <textarea id="Message" class="form-control" value="" name="note">023PO/1RT/2021</textarea>
              </div>
              <div class="col-md-4 ">
                <label for="Attachment" class="form-label">Attachment</label>
                <input class="form-control" id="Attachment" type="file" name="attachments">
              </div>
              <!-- <input type="text" name="order_id" hidden/> -->
            </div>
            <div class="mt-4 text-end pe-5 mb-5">
              <button type="button" class="btn btn-outline-danger btn-sm">Cancel</button>
              <button class="btn btn-success btn-sm">Save</button>
            </div>
          </div>
      </form>
      <!-- end isi form  -->
    </div>
    
  </div>
  <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3"></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <!-- <script type="text/javascript">
      $('.addmoredata').on('click',function() {
          addmoredata();
      });
      function addmoredata(){
        var moredata='<div><div class="row"><div class="col-2"><label for="disabledSelect" class="form-label mt-2 f-14 f-blue">Product</label><select id="disabledSelect" class="form-select f-13 f-blue" name="select_product"><option>Produk 1</option><option>Produk 2</option> <option>Produk 4</option><option>Produk 5</option></select></div> <div class="col-2 mt-2"><label for="disabledTextInput" class="form-label f-14 f-blue ">Description</label><input type="text" id="disabledTextInput" class="form-control f-13 f-blue " placeholder="-" name="desc"></div> <div class="col-1 mt-2 text-right"><label for="disabledTextInput" class="form-label f-14 f-blue">Qty</label><input type="text" id="disabledTextInput" class="form-control f-13 f-blue" placeholder="000" name="qty"></div><div class="col-1 mt-2"><label for="disabledTextInput" class="form-label f-14 f-blue">units</label><select id="disabledSelect" class="form-select f-13 f-blue" name="units"><option>1</option><option>2</option><option>4</option><option>5</option></select></div><div class="col-2 mt-2 text-right"><label for="disabledTextInput" class="form-label f-14 f-blue">Unit Price (in IDR)</label><input type="text" id="disabledTextInput" class="form-control f-13 text-right" placeholder="0,00" name="units_price"></div><div class="col-1 mt-2"><label for="disabledTextInput" class="form-label f-14 f-blue">Tax</label><select id="disabledSelect" class="form-select f-13 f-blue" name="tax"><option>20%</option><option>30%</option><option>40%</option><option>50%</option></select></div> <div class="col-2 mt-2 text-right"><label for="disabledTextInput5" class="form-label f-14 f-blue ">Amount in IDR</label><input type="text" id="disabledTextInput5" class="form-control f-13 text-right" placeholder="0,00" name="amount"></div><div class="col-1 pt-5 cross"><i class="fal fa-times-square ms-4"></i></div></div></div></div>';
            $('.moredata').append(moredata);
          };
          $('.remove').live('click',function(){
              $(this).parent().parent().parent().remove();    
          });
  </script> -->

  <script>
    $(document).ready(function() {
        var id = 0, sumVal=0;
        $('#tambah').click(function() {
          ++id;
          var product=$(` 
            <div class="mt-3"> 
              <div>
                  <div class="row">
                      <div class="row" id="pelatihan`+id+`">
                        <div class="col-md-2">
                          <label for="disabledSelect" class="form-label f-14 f-blue d-md-none">Product</label>
                          <input type="text" id="disabledTextInput" class="form-control f-13" placeholder="Produk..." name="select_product[`+id+`]">
                        </div>
                        <div class="col-md-2">
                          <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Description</label>
                          <textarea type="text" rows="1" id="disabledTextInput" class="form-control f-13 "
                            placeholder="-" name="desc[`+id+`]"></textarea>
                        </div>
                        <div class="col-md-1">
                          <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Qty</label>
                          <input type="text" id="disabledTextInput" class="form-control f-13" placeholder="..349" name="qty[`+id+`]">
                        </div>
                        <div class="col-md-1">
                          <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Units</label>
                          <input type="text" id="disabledTextInput" class="form-control f-13 text-right" placeholder="Ton" name="units[`+id+`]">
                          </div>
                        
                        <div class="col-md-1">
                          <label for="disabledTextInput" class="form-label m-0 f-blue d-md-none">Tax</label>
                          <select type="text" id="disabledTextInput" class="form-select f-13" placeholder="PPN" name="subtax[`+id+`]">
                              <option  value=".00" >Free Tax</option>
                              <option selected value=".1">10%</option>
                              <option selected value=".2">20%</option>
                              <option selected value=".3">30%</option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <label for="disabledTextInput" class="form-label f-14 f-blue d-md-none">Unit Price (in IDR)</label>
                          <input type="text" id="disabledTextInput" class="form-control f-13 text-right"
                            placeholder="23.000,00" name="units_price[`+id+`]">
                        </div>
                        <div class="col-md-2">
                          <label for="disabledTextInput5" class="form-label f-14 f-blue d-md-none">Amount in IDR</label>
                          <input type="text" id="disabledTextInput5" class="form-control f-13 text-right"
                            placeholder="445.027.000.00" name="amount[`+id+`]">
                        </div>
                        <div class="col-md-1 mt-3 mt-md-0">
                          <button type="button" class="btn btn-sm btn-outline-danger rounded remove" id="hapus"><i class="fas fa-times"></i></button>
                        </div>
                      </div>
                    </div>
              </div>
            </div>`)
            $('#form_dinamis').append(product)
            product.find('#hapus').click(function(){
              product.remove();
              $(".row").keyup()      
            })
        }) 
        $(".row").keyup(function(){
          var subtotal = 0;
          var taxtot = 0;
          var totakhir = 0;
          for (var i=0 ; i<=id; i++ ) {
            if(
                !$('#pelatihan'+i).length
              )
              {
                continue;
              } 
            var nilai1 = $('input[name="qty['+i+']"]').val();
            var nilai2 = $('input[name="units_price['+i+']"]').val();
            var ppn = $('select[name="subtax['+i+']"]').val();
            var hasil = parseFloat(nilai1) * parseFloat(nilai2);
            subtotal += hasil;
            var subtax = parseFloat(ppn) * parseFloat(hasil);
            subtax=Math.round(subtax*10)/10
            taxtot += subtax;
            totakhir = parseFloat(subtotal) + parseFloat(taxtot);
            if (hasil) {
              $('input[name="amount['+i+']"]').val(hasil);
            }
            else {
              $('input[name="amount['+i+']"]').val(0);
            }
           
          }

          if (subtotal){
            $("#subtotal").html("Rp. "+ numberWithCommas(subtotal)+",00")
          }
          else{
            $("#subtotal").html("Rp. 00,00")
          }
          $('input[name="subtotal"]').val(subtotal);

          if (taxtot){
            $("#taxppn").html("Rp. "+ numberWithCommas(taxtot)+",00")
          }
          else{
            $("#taxppn").html("Rp. 00,00")
          }
          $('input[name="tax"]').val(taxtot);

          if (totakhir){
            $("#totalakhir").html("Rp. "+ numberWithCommas(totakhir)+",00")
          }
          else{
            $("#totalakhir").html("Rp. 00,00")
          }
          $('input[name="totalakhir"]').val(totakhir);
        })
        // $('#hapus').click(function () {
        //   $(".row").keyup()
        // })
        // $('#hapus').click(function(e) {
        //   e.preventDefault()
        //   console.log($(this).parent().attr('id'))
        //     $('#pelatihan'+ id).remove();
        //     id--;
        // })
        // $('.remove').live('click', function(){
        //   $('#pelatihan'+ id).parent().parent().remove();
        //   --id;
        // });
        
    })
    // function deleteElement(id) {
    //   var element=document.getElementById('pelatihan'+ id).parentElement.parentElement.parentElement
    //   element.remove();
    //   $(".row").keyup()
    // }
    
    // $(".moredata").keyup(function(){
    //   var bil1 = parseInt($("#bila1").val())
    //   var bil2 = parseInt($("#bila2").val())

    //   var hasil = bil1 * bil2;
    //   $("#hasil2").attr("value",hasil)
      // $("#subtotal").html("Rp. "+ hasil)
    // });
  </script>

  <!-- <script type="text/javascript">
    $(".row").keyup(function(){
      var nilai1 = $('input[name="qty[]"]').val();
      var nilai2 = $('input[name="units_price[]"]').val();
      var hasil = parseFloat(nilai1) * parseFloat(nilai2);
      $('input[name="amount[]"]').val(hasil);
      // $("#subtotal").html("Rp. "+ hasil)
    });
  </script> -->

  <script>
    function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


 </body>
 </html>
