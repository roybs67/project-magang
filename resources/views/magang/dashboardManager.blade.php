@extends('layouts.app2')
@section('content')

<div>
<div class="bg-white m-content"> 
      <div class="row">
      </div>
<h3 style="text-align:center;margin-top:25px;color: #0A2F5A;" href="{{route('newpurchase')}}">Overall Progress</h3>
<div class="container2">
    <div id="chartNilai" style="height: 300px; width: 100%;"></div>
</div>

<table style="margin-top:25px;">
  <tr>
    <th>Project List</th>
    <th>Progress</th>
    <th>Company</th>
    <th>Due Date</th>
  </tr>
  @foreach ($purchase as $cust)
  <tr>
    <td><a href="" style="color:black;text-decoration:none">Project 0{{ $cust->id}}</a></td>
    <td><a href="" style="color:black;text-decoration:none">{{ $cust->progresss}}%</a></td>
    <td><a href="" style="color:black;text-decoration:none">{{ $cust->vendor}}</a></td>
    <td><a href="" style="color:black;text-decoration:none">{{ $cust->due_date}}</a></td>
  </tr>
  @endforeach
</table>
<br>
<br>


<!-- <table>
<h3 style="text-align:center;margin-top:30px;color: #0A2F5A;">Upcoming Project</h3>
  <tr>
    <th>Project List</th>
    <th>Company</th>
    <th>Start Date</th>
  </tr>
  <tr>
    <td>Project 11</td>
    <td>PT PLEDIS</td>
    <td>22/01/2022</td>
  </tr>
  <tr>
    <td>Project 12</td>
    <td>PT JYP</td>
    <td>09/02/2022</td>
  </tr>
  <tr>
    <td>Project 13</td>
    <td>PT STARSHIP</td>
    <td>29/02/2022</td>
  </tr>
  <tr>
    <td>Project 14</td>
    <td>PT LOEN</td>
    <td>13/04/2022</td>
  </tr>
  <tr>
    <td>Project 15</td>
    <td>PT YG</td>
    <td>25/05/2022</td>
  </tr>
</table> -->
</div>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- <script src="{{asset('assets/js/bar.js')}}" type="text/javascript" ></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<script>
  const chart = Highcharts.chart('chartNilai', {
      // title: {
      //     text: 'Chart.update'
      // },
      // subtitle: {
      //     text: 'Plain'
      // },
      xAxis: {
          categories: {!!json_encode($categories)!!}
      },
      series: [{
          type: 'bar',
          colorByPoint: true,
          data:  {!!json_encode($data)!!},
          showInLegend: false
      }]
  });

  document.getElementById('plain').addEventListener('click', () => {
      chart.update({
          chart: {
              inverted: false,
              polar: false
          },
          subtitle: {
              text: 'Plain'
          }
      });
  });
</script>
@endsection
