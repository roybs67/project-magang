<!DOCTYPE html>
<html>
<head>
  <title>Detail Materialist</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSEdit2.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">
  
  
</head>
<body class="bg-body">
    <!-- <header class="sticky-top">
        <nav class="navbar navbar-expand-lg navbar-dark color-nav">
          <div class="container-fluid mx-4">
            <a class="navbar-brand" href="#">
              <img src="{{asset('Logo CBA 50.png')}}" alt="" width="74" height="44" class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span></button>
    
            <div class="collapse navbar-collapse ms-3" id="navbarText">
              <ul class="navbar-nav gap-2 gap-lg-0">
                <li class="nav-item rounded py-0">
                  <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                    href="#"><i
                      class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
                </li>
                <li class="nav-item rounded ms-lg-4">
                  <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                    href="#"><i
                      class="fal fa-shopping-cart me-3"></i>New Order</a>
                </li>
                <li class="nav-item rounded ms-lg-4">
                  <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item active" aria-current="page"
                    href="#"><i
                      class="far fa-file-alt me-3"></i>Materialist</a>
                </li>
              </ul>
              <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
                <ul class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
                  <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
                  {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
                  </li>
                  <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50" height="50"></li>
                </ul>
                <a class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
              </div>
            </div>
          </div>
        </nav>
    </header> -->
    <!-- content -->
    <div class="bg-white"> 
        <div class="row mx-1">
            <!-- head content  -->
            <!-- <div class="col-12 pt-3">
                <div class="d-flex d-md-block flex-column-reverse">
                <button
                    class="float-none float-md-end btn btn-sm border border-2 border-color rounded d-flex justify-content-center justify-content-md-between align-items-center gap-2">
                    <i class="fal fa-chevron-left icon-color"></i> Back
                </button>
                <h6 class="f-24 f-blue"> {{$purchase->transaction_no}}<i class="far fa-file-alt f-blue ms-4"></i></h6>
                </div>
            </div>
            <div class="hr">
                <hr>
            </div> -->
            <!-- end head content  -->
            <!-- body content  -->
            <div class="col m-3">
                <div class="m-3">
                    <div class="row">
                        <!-- kop -->
                        <div class="col-8 row">
                            <img src="{{asset('Logo CBA 50.png')}}" class="col-3">
                            <div class="col">
                                <span><strong>Citra Banjar Abadi</strong> </span><br>
                                <span><strong>Fabricator. Contractor & Suplier</strong></span><br>
                                <span>Workshop : Jl. Raya Otonom No. 157, Ds Situ Teratai, Kec. Cikande</span><br>
                                <span>Telp/fax : (0254) 404608</span><br>
                                <span>Email : citrabanjar@gmail.com</span>
                            </div>
                        </div>
                        <div class="col-4 text-end">
                            
                            <span >Dokumen No : 0{{$purchase->id}}</span><br>
                            <span >Revisi No : 0{{ $purchase->vendor_ref_no}}</span>
                        </div>
                        <!-- end kop -->
                        <!-- tabel  -->
                        <table class="table table-bordered mt-5">
                            <!-- <thead> -->
                                <tr>
                                    <th colspan="3" rowspan="2">Packing List</th>
                                    <td colspan="3">Revisi ke</td>
                                    <td colspan="5">: 0{{$purchase->materialist->num_rev}}</td>
                                </tr>
                                <tr>
                                    <td colspan="3">Packing list No</td>
                                    <td colspan="5">: {{$purchase->materialist->pack_list_no}}</td>
                                </tr>
                                <tr>
                                    <th colspan="3" rowspan="2">TOWER TYPE {{$purchase->materialist->tower_type}}
                                    </th>
                                    <td colspan="3">Tanggal</td>
                                    <td colspan="5">:<span id="tanggalwaktu"></span></td>
                                </tr>
                                <tr>
                                    <td colspan="3">Pemilik</td>
                                    <td colspan="5">: {{$purchase->vendor}}</td>
                                </tr>
                               
                                <tr>
                                    <!-- <td rowspan="2">NO</td> -->
                                    <td>NO.</td>
                                    <td rowspan="2">SECTION</td>
                                    <td colspan="3" rowspan="2">PROFIL</td>
                                    <td>LENGTH</td>
                                    <td>QTY</td>
                                    <td>QTY</td>
                                    <td>TOTAL</td>
                                    <td rowspan="2">Done</td>
                                </tr>
                              
                                <tr>
                                  <td>Marking</td>
                                  <td>(mm)</td>
                                  <td>(Pcs)</td>
                                  <td>Unit</td>
                                  <td>(QTY)</td>
                                </tr>
                                @foreach ($purchase ->materialistpro as $cust)
                                <tr>
                                  <!-- <td>{{ $cust->id}}</td> -->
                                  <td>{{ $cust->mark_no}}</td>
                                  <td>{{ $cust->section}}</td>
                                  <td>{{ $cust->profile1}}</td>
                                  <td>{{ $cust->profile2}}</td>
                                  <td>{{ $cust->profile3}}</td>
                                  <td>{{ $cust->length}}</td>
                                  <td>{{ $cust->qty}}</td>
                                  <td>{{ $cust->qty_units}}</td>
                                  <td>{{ $cust->tot_qty}}</td>
                                  <td>{{ $cust->done}}</td>
                                </tr>
                                @endforeach
                            <!-- </thead> -->
                        </table>
                        <table class="billie">
                            <!-- <thead> -->
                                <tr>
                                    <td>Total Qty</td>
                                    <td >Total Done</td>
                                    <td >Progress</td>
                                </tr>
                                <tr>
                                    <td >{{$purchase->materialist->total_qty}}</td>
                                    <td >{{$purchase->materialist->total_done}}</td>
                                    <td >{{$purchase->materialist->progress}}%</td>
                                </tr>
                                
                            <!-- </thead> -->
                        </table>
                        <div class="row mt-1">
                        </div>
                        
                        <table class="table table-bordered mt-5 tabs" >
                          <!-- <thead> -->
                              <tr>
                                  <td >Acknowledged</td>
                                  <td >Warehouse</td>
                                  <td >Recipient</td>
                              </tr>
                             
                              <tr class="ttabs">
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                            <td>{{$purchase->materialist->acknowledged}}</td>
                                <td>{{$purchase->materialist->warehouse}}</td>
                                <td>{{$purchase->materialist->recipient}}</td>
                          </tr>
                          </table>
                                

                        <!-- end tabel  -->
                    </div>
                        <!-- </thead> -->



                </div>
                
                

            </div>
            <!-- end body content  -->
        </div>
    </div>
    <!-- <div class="footer sticky-bottom d-flex flex-column justify-content-center">
        <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
          <div class="float-md-end float-none text-center text-white list">
            <i class="fal fa-envelope f me-3"></i>
            <i class="fal fa-phone-alt f me-3" ></i>
            <i class="fal fa-map-marker-alt f "></i>
          </div>
          <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
            Abadi All Rights Reserved.
          </span>
        </div>
    </div> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script>
      var dt = new Date();
      document.getElementById("tanggalwaktu").innerHTML = dt.toLocaleString();
    </script>

    <script type="text/javascript">
       window.print();
  </script>
</body>
</html>