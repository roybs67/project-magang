<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css"> 
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSLanding.css') !!}">
</head>
<header>
    <!-- head -->
    <nav class="navbar navbar-dark navbar-expand-lg" style="background-color: #5DA0EF;">
      <div class="container-fluid mx-5">
        <a class="navbar-brand" href="#">
          <img src="Logo CBA 50.png" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>

      <div class="collapse navbar-collapse ms-3" id="navbarText">
        <ul class="navbar-nav">
          <li class="nav-item text-white border border-2 rounded">
            <a class="nav-link text-white f-14" aria-current="page" href="{{route('detail.purchase')}}"><i class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
          </li>
          <li class="nav-item border border-2 rounded ms-4">
            <a class="nav-link text-white f-14" aria-current="page" href="{{route('newpurchase')}}"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item text-white border border-2 rounded ms-4">
              <a class="nav-link text-white f-14" aria-current="page" href="#"><i class="far fa-file-alt me-3 active"></i>Materialist</a>
              </li>
              <li class="nav-item border border-2 rounded ms-4">
              <a class="nav-link text-white f-14"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <ul class="navbar-nav ms-auto">
            <li class="nav-item text-white pt-1 text-end"><span class="navbar-text text-white f-18">
            {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
            </ul>
          </div>
          </div>
    </nav>
</header>  
<body  class="bg-body">
<div class="bg-white m-content"> 
      <div class="row">
      <h6 class="f-24 ms-5 pt-3 f-blue col"> Materialist<i class="far fa-clipboard-list-check me-3 f-blue ms-4"></i></h6>
      <!-- <div class="hr mx-5">
      <hr>
    </div> -->

    <div class="d-flex">
  <div class="hr mx-auto">
    <hr>
  </div>
</div>
    <h5 class="h5-cus ms-5 pt-3 f-blue col" href="{{route('newpurchase')}}">Purchase Transaction List</h5>
      <form action="/materialis" method="GET" class="col-md-6">
            <div class="card">
                <div class="input-box"> <input type="text" class="form-control" placeholder="Search" name="cari"> <i class="fa fa-search"></i></div>
            </div>
      </form>
</div>

<table>
    <th>Date</th>
    <th>Transaction No.</th>
    <th>Vendor</th>
    <th>Due Date</th>
    <th>Status</th>
    <th>Progress (in %)</th>
    <th>Desc</th>
    <th></th>
@foreach ($purchase as $cust)
  <tr>
    <td><a href="{{url('/detailmaterialist/'.$cust->id)}}" style="color:black;text-decoration:none">{{ $cust->transaction_date}}</a></td>
    <td>{{$cust->transaction_no}}</td>
    <td>{{$cust->vendor}}</td>
    <td>{{$cust->due_date}}</td>
    <td>Ongoing</td>
    <td>{{$cust->progresss}}</td>
    <td>{{$cust->descis}}</td>
    <!-- <td>
      <form action="#" class="col-md-1 mt-3 mt-md-0">
        <div class="col-md-1 mt-3 mt-md-0">
        <button type="submit" class="btn btn-sm btn-outline-danger remove" id="hapus" value="delete" onclick=""><i class="fas fa-times"></i></button>
        </div>
      </form>  
    </td> -->
  </tr>
@endforeach
  <!-- <tr>
    <td>25/05/2021</td>
    <td>Invoice#00002</td>
    <td>PT. ABCDEF</td>
    <td>25/06/2021</td>
    <td>Finished</td>
    <td>100.0</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>02/06/2021</td>
    <td>Invoice#00003</td>
    <td>PT. XYZ Jaya</td>
    <td>02/07/2021</td>
    <td>Finished</td>
    <td>100.0</td>
    <td>Done</td>
  </tr>
  <tr>
    <td>31/06/2021</td>
    <td>Invoice#00004</td>
    <td>PT. Blockberry</td>
    <td>31/07/2021</td>
    <td>Ongoing</td>
    <td>93.0</td>
    <td>-2</td>
  </tr>
  <tr>
    <td>13/07/2021</td>
    <td>Invoice#00005</td>
    <td>PT. Pratama</td>
    <td>10/08/2021</td>
    <td>Ongoing</td>
    <td>75.0</td>
    <td>-6 <i class="far fa-times-square f-red ms-1"></i></td>
  </tr> -->
</table>



<!-- <nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav> -->

<br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>

</div>
<div class="footer <!--fixed-bottom--> d-flex flex-column justify-content-center" style="background-color: #0A2F5A;" >
        <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
            <div class="float-md-end float-none text-center text-white list">
                <i class="fal fa-envelope f me-3" style="color: #ffff"></i>
                <i class="fal fa-phone-alt f me-3" style="color: #ffff"></i>
                <i class="fal fa-map-marker-alt f " style="color: #ffff"></i>
            </div>
            <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
                Abadi All Rights Reserved.
            </span>
        </div>
    </div>

<!-- <footer>
  <div class="text-center p-5 f f-5 foot" style="background-color: #0A2F5A;">
    © 2021, PT. Citra Banjar Abadi All Rights Reserved.
    <i class="fal fa-envelope f p1"></i>
    <i class="fal fa-phone-alt f p2" ></i>
    <i class="fal fa-map-marker-alt f p3"></i>
  </div>
</footer> -->


<!-- Copyright -->

<!-- End of .container -->
<!-- 
<div id="app" class="container">  
<ul class="page">
    <li class="page__btn"><span class="material-icons">chevron_left</span></li>
    <li class="page__numbers"> 1</li>
    <li class="page__numbers active">2</li>
    <li class="page__numbers">3</li>
    <li class="page__numbers">4</li>
    <li class="page__numbers">5</li>
    <li class="page__numbers">6</li>
    <li class="page__dots">...</li>
    <li class="page__numbers"> 10</li>
    <li class="page__btn"><span class="material-icons">chevron_right</span></li>
  </ul>
</div> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <!-- <a href="https://www.youtube.com/channel/UC7hSS_eujjZOEQrjsda76gA/videos" target="_blank" id="ytd-url">My YouTube Channel</a> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
<!-- // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional -->
<script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js"></script>
<!-- TODO: Add SDKs for Firebase products that you want to use
https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-analytics.js"></script>

</body>
</html>