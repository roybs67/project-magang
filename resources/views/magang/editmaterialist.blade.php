
<html>
<head>
  <title>Edit Materialist</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/CSSEdit.css') !!}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
  <link href="JS.js" rel="stylesheet">
  

</head>
<body class="bg-body">
  <header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="{{asset('Logo CBA 50.png')}}" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('detail.purchase')}}"><i
                  class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i
                  class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('dashboard.materi')}}"><i
                  class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
              {{auth()->user()->username}} <br> <span class="f-14">{{auth()->user()->role}}</span>
              </li>
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="{{asset('Profile.png')}}" alt="" width="50"
                  height="50"></li>
            </ul>
            <a href="{{route('logout')}}" class="col-auto nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <!-- content -->
  <form action="/{{$purchase->id}}/editmaterialist" method="POST" enctype="multipart/form-data">
     {{ csrf_field() }}
  <div class="bg-white m-content pb-5" > 
    <div class="row mx-4 ">
      <!-- head content  -->
      <div class="col-12 pt-3">
        <div class="d-flex d-md-block flex-column-reverse">
          <button
            class="float-none float-md-end btn btn-sm border border-2 border-color rounded d-flex justify-content-center justify-content-md-between align-items-center gap-2">
            <i class="fal fa-chevron-left icon-color"></i><a href="/detailmaterialist/{{$purchase->id}}" style="color:black;text-decoration:none">Back</a>
          </button>
          <h6 class="f-24 f-blue"> Edit Materialist<i class="far fa-file-alt f-blue ms-4"></i></h6>
        </div>
      </div>
      <div class="hr">
        <hr>
      </div>
      <span class="f-18 f-blue">Materialist Information</span>
      
      <!-- end head content  -->
      <!-- isi form  -->
      <div class="row">
        <!-- row 1 -->
            <div class="col-md-5 mt-2">
              <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor</label><br>
              <span class="f-13 f-blue ">{{ $purchase->vendor}}</span>
            </div>
            <div class="col-md-5 mt-2">
              <label for="disabledTextInput1" class="form-label f-14 fw-bold">Transaction Date:</label><br>
              <span class="f-13 f-blue">{{ $purchase->transaction_date}}</span>
            </div>
            <div class="col-md-2 mt-2">
              <label for="disabledTextInput1" class="form-label f-14 fw-bold">Tower Type</label><br>
              <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="AA2+3" name=" tower_type"  value="{{$purchase->materialist->tower_type}}" >
              <!-- <select id="disabledSelect" class="form-select f-13 f-blue">
                <option>AA2+3</option>
              </select> -->
            </div>
      </div>
      <!-- end row 1 -->
      <!-- row 2 -->
      <div class="row">
        <div class="col-md-5 mt-2">
          <label for="disabledSelect1" class="form-label f-14 text fw-bold">Email</label><br>
          <span class="f-13 f-blue ">{{ $purchase->email}}</span>
        </div>
        <div class="col-md-5 mt-2">
          <label for="disabledTextInput1" class="form-label f-14 fw-bold">Transaction No.:</label><br>
          <span class="f-13 f-blue">{{ $purchase->transaction_no}}</span>
        </div>
        <div class="col-md-2 mt-2">
          <label for="disabledTextInput1" class="form-label f-14 fw-bold">Packing List No.</label><br>
          <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="CBA/PL-020/V/2021" name="pack_list_no" value="{{$purchase->materialist->pack_list_no}}">
        </div>
      </div>
      <div class="row">
        <div class="col-md-5 mt-2">
          <label for="disabledSelect1" class="form-label f-14 text fw-bold">Vendor Address</label><br>
          <span class="f-13 f-blue ">{{ $purchase->vendor_adress}}</span>
        </div>
        <div class="col-md-5 mt-2">
          <label for="disabledTextInput1" class="form-label f-14 fw-bold">Vendor Ref.No.</label><br>
          <span class="f-13 f-blue">{{ $purchase->vendor_ref_no}}</span>
        </div>
        <div class="col-md-2 mt-3">
          <label for="disabledTextInput1" class="form-label f-14 ms-auto fw-bold">Number of Revision:</label><br>
          <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="1" name="num_rev"  value="{{$purchase->materialist->num_rev}}" >
        </div>
      </div>
      <form >
      <div class="row my-3">
        <div class="col-md-12 mt-3">
          <h7 class="f-14">Note:</h7><br>
          <ul class="f-13">
            <li>The attachment must be in CSV or XLSX format.</li>
            <li>Data inside must including Marking No., Section, Profile, Length, Quantity (Qty), Qty Unit and Total Qty.</li>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
              Import
            </button>
          </ul>
        </div> 
       
      </div>
      </form>
      <div class="row">
        <span class="f-18 f-blue">Materialist Data</span>
        <div class="table-responsive">
          <table class="table table-borderless mt-2">
            <thead class="header-color">
              <tr class="tab custom-rounded">
                <th class="f-14 f-blue" scope="col">Marking No.</th>
                <th class="f-14 f-blue" scope="col">Section</th>
                <th class="f-14 f-blue text-right" scope="col" colspan="2">Profile</th>
                <th class="f-14 f-blue text-right" scope="col" colspan="2" >Length(in mm)</th>
                <th class="f-14 f-blue text-right" scope="col" colspan="">Qty(in pcs)</th>
                <th class="f-14 f-blue text-right" scope="col" colspan="">Qty Units</th>
                <th class="f-14 f-blue  text-right" scope="col">Total Qty</th>
                <th class="f-14 f-blue  text-right" scope="col">Last Done</th>
                <th class="f-14 f-blue text-right" scope="col">Done</th>
              </tr>
            </thead>
            <tbody>
            
            @foreach ($purchase->materialistpro as $key=>$item)
              <tr>
                <td class="f-13 f-blue" scope="col" name="mark_no[{{$key}}]">{{$item->mark_no}}</td>
                <td class="f-13 f-blue" name="section[{{$key}}]">{{$item->section}}</td>
                <td class="f-13 f-blue" name="profile1[{{$key}}]">{{$item->profile1}}</td>
                <td class="f-13 f-blue" name="profile2[{{$key}}]">{{$item->profile2}}</td>
                <td class="f-13 f-blue" name="profile3[{{$key}}]">{{$item->profile3}}</td>
                <td class="f-13 f-blue text-right" name="length[{{$key}}]">{{$item->length}}</td>
                <td class="f-13 f-blue text-right" name="qty[{{$key}}]">{{$item->qty}}</td>
                <td class="f-13 f-blue text-right" name="qty_units[{{$key}}]">{{$item->qty_units}}</td>
                <td class="f-13 f-blue text-right" name="tot_qty[{{$key}}]">{{$item->tot_qty}}</td>
                <!-- <td class="f-13 f-blue text-right" name="lastdone[{{$key}}]">{{$item->lastdone}}</td> -->
                <td class="f-13 f-blue text-right" id="last_done[{{$key}}]">{{$item->done ? $item->done : '0'}}</td>
                <td class="f-13 f-blue text-right d-flex justify-content-end">
                  <input type="number" id="d" class="done form-control f-13"
                    placeholder="1"
                    style="width: 3.5rem;min-width: 3.5rem;" min="0" max="{{$item->tot_qty-$item->done}}" name="done[{{$key}}]" value="0"
                    >
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="row justify-content-end">
        <div class="col-md-5 mt-2 text-end">
          <div class="row justify-content-end align-items-center">
            <label for="disabledTextInput" class="col-9 f-14 f-blue text-start">Total Qty</label><br>
            <label for="disabledTextInput" class="col-3 f-14 f-blue text-end">{{$purchase->materialist->total_qty}}</label><br>
  
            <label for="disabledSelect" class="col-9 form-label mt-1 f-14 f-blue text-start">Total Done</label><br>
            <span class="col-3 f-14 f-blue text-end" id="total_done">{{($purchase->materialistpro->sum('done'))}}</span><br>
            <input type="text" name="total_done" value="{{($purchase->materialistpro->sum('done'))}}" />
            <input type="text" name="desci" value="{{($purchase->materialist->total_qty)-($purchase->materialistpro->sum('done'))}}"/>
  
            @if ($purchase->materialist->total_qty)
            <label for="disabledSelect"
            class="col-9 form-label  mt-1 f-18 f-blue text-start"><b>Progress</b></label>
            <span class="col-3 f-18 f-blue text-end"><b id="progress">{{number_format((float)($purchase->materialistpro->sum('done')/$purchase->materialist->total_qty)*100, 2, '.', '');}}%</b>
            <input type="text" name="progress" value="{{number_format((float)($purchase->materialistpro->sum('done')/$purchase->materialist->total_qty)*100, 2, '.', '');}}" hidden/>
            @endif
          </div>
        </div>
        <div class="row mt-3">
          <!-- row 1 -->
          <div class="col-md-4 mt-2">
            <label for="disabledSelect" class="form-label f-14">Acknowledged</label>
            <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="Rudini Anwar" name="acknowledged" value="{{$purchase->materialist->acknowledged}}" >
          </div>
          <div class="col-md-4 mt-2">
            <label for="disabledTextInput" class="form-label f-14 ">Warehouse</label>
            <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="Didi Rukadiana" name="warehouse" value="{{$purchase->materialist->warehouse}}" >
          </div> 
          <div class="col-md-4 mt-2">
            <label for="disabledTextInput" class="form-label f-14 ">Recipient</label>
            <input type="text" id="disabledTextInput" class="form-control f-13 " placeholder="-" name="recipient" value="{{$purchase->materialist->recipient}}">
          </div> 
        </div>
        <div class="row justify-content-end mt-3">
          <div class="col-sm-auto">
            <div class="row gap-2 gap-md-1 gap-lg-2">
              <button type="button" class="col-sm-auto btn btn-sm btn-outline-danger">Cancel</button>
              <button class="col-sm-auto btn btn-sm btn-success">Save</button>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
  </form>
  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <form class="modal-content" action="{{ route('materialimport',['id'=>$purchase->id])}}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="Attachment" class="form-label f-18 f-blue">Materialist Data Upload</label>
          <input class="form-control" id="Attachment" type="file" name="file" required="required">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Input</button>
      </div>
    </form>
  </div>
</div>
  <div class="footer sticky-bottom d-flex flex-column justify-content-center">
    <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
      <div class="float-md-end float-none text-center text-white list">
        <i class="fal fa-envelope f me-3"></i>
        <i class="fal fa-phone-alt f me-3"></i>
        <i class="fal fa-map-marker-alt f "></i>
      </div>
      <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
        Abadi All Rights Reserved.
      </span>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
  $(document).ready(function() {
        var id = {{$purchase->materialistpro->count('done')-1}};
        $(".done").each(function(){
          $(this).keyup(function(){
            this.style.width = ((this.value.length + 1) * 8 + (8*4)) + 'px';
            var total_done = 0;
            var cobaaja = 0;
            var progress = 0;
            var last_done2=0;
            var last_done1=0;
            var nilai3 = 0;
            for (var i=0 ; i<=id; i++ ) {
              var nilai1 = $('input[name="done['+i+']"]').val();
              //nilai3 += parseInt(nilai3) + parseInt(nilai1);
              total_done += parseInt(nilai1);
              //last_done1 += parseInt(nilai1) + parseInt(nilai3);
              //last_done2 +=last_done1;
              //$('input[name="lastdone['+i+']"]').val(total_done);
              console.log(total_done);
            }
            total_done += {{$purchase->materialistpro->sum('done')}}
            var nilai2 = {{$purchase->materialist->total_qty}};
            progress = (total_done/nilai2)*100;

            cobaaja = total_done+nilai2;

            $("#progress").html(parseFloat(progress).toFixed(2)+'%');
            $('input[name="progress"]').val(parseFloat(progress).toFixed(2));
            $("#total_done").html(total_done);
            $('input[name="total_done"]').val(total_done);
          })
          $(this).mouseup(function(){
            $(this).keyup();
          })
        })
    })
</script>
</body>
</html>