@extends('layouts.app2')
@section('content')

<div>
<div class="bg-white m-content"> 
      <div class="row">
      </div>
<h3 style="text-align:center;margin-top:25px;color: #0A2F5A;" href="{{route('newpurchase')}}">Your Project Progress</h3>
<div class="container2">
    <div id="chartNilai" style="height: 300px; width: 100%;"></div>
</div>

<table style="margin-top:25px;">
  <tr>
    <th>Project List</th>
    <th>Star Date</th>
    <th>Deadline</th>
    <th>Cost(in IDR)</th>
    <th>Progress</th>
  </tr>
  @foreach ($purchase->project as $cust)
  <tr>
    <td><a href="" style="color:black;text-decoration:none">Project 0{{ $cust->id}}</a></td>
    <td><a href="" style="color:black;text-decoration:none">{{ $cust->transaction_date}}</a></td>
    <td><a href="" style="color:black;text-decoration:none">{{ $cust->due_date}}</a></td>
    <td><a href="" style="color:black;text-decoration:none">{{number_format($cust->totalakhir, 0, '' , '.')}},00</a></td> 
    <td><a href="" style="color:black;text-decoration:none">{{ $cust->progresss}}%</a></td>
  </tr>
  @endforeach
</table>




<br>
<br>
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<!-- <script src="{{asset('assets/js/bar.js')}}" type="text/javascript" ></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<script>
  const chart = Highcharts.chart('chartNilai', {
      // title: {
      //     text: 'Chart.update'
      // },
      // subtitle: {
      //     text: 'Plain'
      // },
      xAxis: {
          categories: {!!json_encode($categories)!!}
      },
      series: [{
          type: 'bar',
          colorByPoint: true,
          data:  {!!json_encode($data)!!},
          showInLegend: false
      }]
  });

  document.getElementById('plain').addEventListener('click', () => {
      chart.update({
          chart: {
              inverted: false,
              polar: false
          },
          subtitle: {
              text: 'Plain'
          }
      });
  });
</script>
@endsection
