<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css"> 
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style.css') !!}">
</head>
<body class="bg-body">
<header class="sticky-top">
    <!-- head -->
    <nav class="navbar navbar-expand-lg navbar-dark color-nav">
      <div class="container-fluid mx-4">
        <a class="navbar-brand" href="#">
          <img src="Logo CBA 50.png" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span></button>

        <div class="collapse navbar-collapse ms-3" id="navbarText">
          <ul class="navbar-nav gap-2 gap-lg-0">
            <li class="nav-item rounded py-0">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item active" aria-current="page"
                href="{{route('detail.purchase')}}"><i class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
            </li>
            @if (auth()->user()->role=="Super Admin")
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('dashboard.materi')}}"><i class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
            @elseif (auth()->user()->role=="Admin")
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('newpurchase')}}"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item" aria-current="page"
                href="{{route('dashboard.materi')}}"><i class="far fa-file-alt me-3"></i>Materialist</a>
            </li>
            <li class="nav-item rounded ms-lg-4">
              <a class="text-start text-lg-center btn btn-outline-light f-14 nav-link custom-item"
                aria-current="page" href="{{route('approval')}}"><i class="far fa-user-lock me-3"></i>Approval</a>
            </li>
            @endif
          </ul>
          <div class="row ms-lg-auto align-items-center my-3 my-lg-0">
            <ul class="col-auto navbar-nav justify-content-end d-md-flex flex-row-reverse flex-lg-row align-items-center">
              <li class="nav-item text-white pt-1 text-lg-end"><span class="navbar-text text-white f-18">
              {{auth()->user()->username}}  <br> <span class="f-14">{{auth()->user()->role}} </span>
              </li>
              @if (auth()->user()->role=="Super Admin")
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="Profile.png" alt="" width="50" height="50">
              </li>
              @elseif (auth()->user()->role=="Manager")
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="Prof Manager.png" alt="" width="50" height="50">
              </li>
              @elseif (auth()->user()->role=="Customer")
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="Prof Customer.png" alt="" width="50" height="50">
              </li>
              @else
              <li class="nav-item ms-3 me-3 me-lg-0"><img src="Profile.png" alt="" width="50" height="50">
              </li> 
              @endif
            </ul>
            <a class="col-auto nav-link text-white" aria-current="page" href="{{route('logout')}}"><i class="fal fa-sign-out"></i></a>
          </div>
        </div>
      </div>
    </nav>
  </header>
  @yield('content')  
  <div class="footer <!--fixed-bottom--> d-flex flex-column justify-content-center">
        <div class="py-4 px-5 d-flex flex-column-reverse d-md-block gap-2 justify-content-end align-items-center">
            <div class="float-md-end float-none text-center text-white list">
                <i class="fal fa-envelope f me-3"></i>
                <i class="fal fa-phone-alt f me-3"></i>
                <i class="fal fa-map-marker-alt f "></i>
            </div>
            <span class="text-white text-center footer-text" style="display: block;">© 2021, PT. Citra Banjar
                Abadi All Rights Reserved.
            </span>
        </div>
    </div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
<!-- // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional -->
<script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js"></script>
<!-- TODO: Add SDKs for Firebase products that you want to use
https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-analytics.js"></script>

</body>
</html>