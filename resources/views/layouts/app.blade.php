<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css"> 
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style.css') !!}">
</head>
<body>
<header>
    <!-- head -->
    <nav class="navbar navbar-dark navbar-expand-lg" style="background-color: #5DA0EF;">
      <div class="container-fluid mx-5">
        <a class="navbar-brand" href="#">
          <img src="Logo CBA 50.png" alt="" width="74" height="44" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

      <div class="collapse navbar-collapse ms-3" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item text-white border border-2 rounded">
            <a class="nav-link text-white f-14" aria-current="page" href="#"><i class="fal fa-chart-pie-alt me-3"></i>Dashboard</a>
          </li>
          <li class="nav-item border border-2 rounded ms-4">
            <a class="nav-link text-white f-14" aria-current="page" href="#"><i class="fal fa-shopping-cart me-3"></i>New Order</a>
            </li>
            <li class="nav-item text-white border border-2 rounded ms-4">
              <a class="nav-link text-white f-14" aria-current="page" href="#"><i class="far fa-file-alt me-3"></i>Materialist</a>
              </li>
          </ul>
          <ul class="navbar-nav ms-auto">
            <li class="nav-item text-white pt-1 text-end"><span class="navbar-text text-white f-18">
              Isma'il Muhammad Falih <br> <span class="f-14">Admin</span>
             </span></li>
             <li class="nav-item ms-3"><img src="Profile.png" alt="" width="50" height="50"></li>
            <li class="nav-item ms-3 mt-2"><a class="nav-link text-white" aria-current="page" href="#"><i class="fal fa-sign-out"></i></a></li>
            </ul>
          </div>
          </div>
    </nav>
</header>  
@yield('content')

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
<!-- // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional -->
<script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js"></script>
<!-- TODO: Add SDKs for Firebase products that you want to use
https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/9.0.2/firebase-analytics.js"></script>

</body>
</html>