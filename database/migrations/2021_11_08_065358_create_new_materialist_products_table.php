<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewMaterialistProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_materialist_products', function (Blueprint $table) {
            $table->id();
            $table->integer('materi_id');
            $table->string('mark_no',100);
            $table->string('section',100);
            $table->string('profile1',100);
            $table->string('profile2',100);
            $table->string('profile3',100);
            $table->string('length',100);
            $table->string('qty',100);
            $table->string('qty_units',100);
            $table->string('tot_qty',100);
            $table->string('done',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_materialist_products');
    }
}
