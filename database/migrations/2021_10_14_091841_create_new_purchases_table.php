<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_purchases', function (Blueprint $table) {
            $table->id();
            $table->string('vendor',100);
            $table->string('email',100);
            $table->text('vendor_adress');
            $table->date('transaction_date');
            $table->date('due_date');
            $table->string('transaction_no',100);
            $table->string('vendor_ref_no',100);
            $table->string('term',100);
            $table->string('subtotal',100);
            $table->string('tax',100);
            $table->string('totalakhir',100);
            $table->text('message');
            $table->text('note');
            $table->string('attachments',1000);
            $table->string('progresss',100);
            $table->string('descis',100);
            $table->int('vendor_id',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_purchases');
    }
}
