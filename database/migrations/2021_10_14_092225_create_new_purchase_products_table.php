<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewPurchaseProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_purchase_products', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('select_product',100);
            $table->string('desc',100);
            $table->string('qty',100);
            $table->string('units',100);
            $table->string('units_price',100);
            $table->string('subtax',100);
            $table->string('amount',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_purchase_products');
    }
}
