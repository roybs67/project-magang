<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewMaterialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_materialists', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->string('tower_type',100);
            $table->string('pack_list_no',100);
            $table->string('num_rev',100);
            $table->string('acknowledged',100);
            $table->string('warehouse',100);
            $table->string('recipient',100);
            $table->string('total_qty',100);
            $table->string('total_done',100);
            $table->string('progress',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_materialists');
    }
}
