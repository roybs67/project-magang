<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewMaterialistProduct extends Model
{
    protected $table = "new_materialist_products";
    protected $primaryKey = "id";
    // use HasFactory;
    protected $fillable = ['id','materi_id','mark_no','section','profile1','profile2','profile3','length','qty','qty_units','tot_qty' ,'done','lastdone'];

    public function purchase()
    {
        return $this->belongsToMany(NewPurchase::class);
    }
}
