<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewMaterialist extends Model
{
    protected $table = "new_materialists";
    protected $primaryKey = "id";
    // use HasFactory;
    protected $fillable = ['id','order_id','tower_type','pack_list_no','num_rev','acknowledged','warehouse','recipient','total_qty','total_done','progress','desci' ];

    public function purchase()
    {
        return $this->belongsTo(NewPurchase::class);
    }
}
