<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewPurchase extends Model
{
    
   
   
    protected $table = "new_purchases";
    protected $primaryKey = "id";
    protected $fillable =['id','vendor','email','vendor_adress','transaction_date','due_date','transaction_no','vendor_ref_no','term','subtotal','tax','totalakhir','message','note','	attachments','progresss','descis','vendor_id'];
     
    public function detail()
    {
        return $this->hasMany('App\Models\Model\NewPurchaseProduct', 'user_id');
    }
    public function materialist()
    {
        
        return $this->hasOne(NewMaterialist::class, 'order_id');
    }
    
    public function materialistpro()
    {
        
        return $this->hasMany(NewMaterialistProduct::class, 'materi_id');
    }

    public function project()
    {
        return $this->belongsTo(User::class, 'vendor_id');
    }
    
}
