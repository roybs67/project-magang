<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewPurchaseProduct extends Model
{
    use HasFactory;
    protected $fillable =['user_id','select_product','desc','qty','units','units_price','subtax','amount'];
}
