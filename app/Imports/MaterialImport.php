<?php

namespace App\Imports;

use App\Models\Model\NewMaterialistProduct;
use Maatwebsite\Excel\Concerns\ToModel;

class MaterialImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $materi_id;
    public function __construct($materi_id) {
        $this->materi_id = $materi_id;
    }
    public function model(array $row)
    {
        return new NewMaterialistProduct([
            'materi_id'=>$this->materi_id,
            'mark_no' => $row[2],
            'section' => $row[3],
            'profile1' => $row[4],
            'profile2' => $row[5],
            'profile3' => $row[6],
            'length' => $row[7],
            'qty' => $row[8],
            'qty_units' => $row[9],
            'tot_qty' => $row[10],
        ]);
    }
}
