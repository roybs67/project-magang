<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use Reminder;
use App\Models\User;
use Mail;

class ForgotPassword extends Controller
{
    public function forgot(){
        return view('magang.forgot');
    }

    public function password(Request $request){
        $user = User::whereEmail($request->email)->first();
        
        if($user == null){
            return redirect()->back()->with(['error'=> 'Email not exists']);
        }

        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user) ? : Reminder::create($user);
        $this->sendEmail($user, $reminder->code);

        return redirect()->back()->with(['success' => 'Reset code sent to your email.']);
    }
    
    public function sendEmail($user, $code){
        Mail::send(
            'email.forgot',
            ['user'=>$user, 'code' =>$code],
            function($message) use ($user){
                $message->to($user->email);
                $message->subject("$user->name, reset your password,");
            }
        );
    }

    public function reset($email, $code){
        $user = User::whereEmail($email)->first();
        
        if($user == null){
           echo 'Email not exists';
        }

        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user);

        if($reminder){
                return view('magang.reset_password_form')->with(['user'=>$user, 'code'=>$code]);
        }else{
            echo 'time expired';
        }
    }

    public function resetPassword(Request $request, $email, $code){
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);

        $user = User::whereEmail($email)->first();
        if($user == null){
           echo 'Email not exists';
        }

        $user = Sentinel::findById($user->id);
        $reminder = Reminder::exists($user);

        if($reminder){
            Reminder::complete($user, $code, $request->password);
            return redirect('/login')->with('success', 'Password reset. Please login with new password.');
        }else{
            echo 'time expired';
        }
    }
}
