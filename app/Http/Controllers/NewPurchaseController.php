<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Model\NewPurchase;
use App\Models\Model\NewPurchaseProduct;
use App\Models\Model\NewMaterialist;
use App\Models\Model\NewMaterialistProduct;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Imports\MaterialImport;
use Maatwebsite\Excel\Facedes\Excel;
use App\Http\Controllers\Controller;
use App\Mail\UserActivation;
use DB;
use Illuminate\Support\Facades\Mail;

class NewPurchaseController extends Controller
{
    public function index()
    {
        return view('magang.newpurchase');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $nm = $data['attachments'];
        $namaFile = $nm->getClientOriginalName();

        $purchase = new  NewPurchase;
        $purchase->vendor = $data['vendor'];
        $purchase->email= $data['email'];
        $purchase->vendor_adress = $data['vendor_adress'];
        $purchase->transaction_date = $data['transaction_date'];
        $purchase->due_date = $data['due_date'];
        $purchase->transaction_no = $data['transaction_no'];
        $purchase->vendor_ref_no = $data['vendor_ref_no'];
        $purchase->term = $data['term'];
        $purchase->subtotal = $data['subtotal'];
        $purchase->tax = $data['tax'];
        $purchase->totalakhir = $data['totalakhir'];
        $purchase->message = $data['message'];
        $purchase->note = $data['note'];
        // $purchase->attachments = $data['attachments'];
        $purchase->attachments = $namaFile;
        $nm->move(public_path().'/pdf',$namaFile);
        $purchase->save();

        $materi = new NewMaterialist;
        $materi->order_id =  $purchase->id;
        $materi->save();
        // dd($materi);

        foreach ($data['select_product'] as $key => $select_product) {
           NewPurchaseProduct::create(
                [
                    'user_id'=> $purchase->id, 
                    'select_product' => $data['select_product'][$key],
                    'desc' => $data['desc'][$key],
                    'qty' => $data['qty'][$key],
                    'units' => $data['units'][$key],
                    'units_price' => $data['units_price'][$key],
                    'subtax' => $data['subtax'][$key],
                    'amount' => $data['amount'][$key]

                ]
            );
        }

        // foreach ($data['order_id'] as $key => $order_id) {
        //     NewMaterialist::create(
        //          [
        //              'order_id'=> $purchase->id, 
        //          ]
        //      );
        //  }
        
        


       

        return redirect() ->back()->with('status', 'Data Berhasil di Input');
    }



    public function detail()
    {
        $purchase = NewPurchase::all();
        //Menyiapkan data untuk chart
        $categories = [];
        $data = [];
        foreach ($purchase as $pur){
            $categories[] = $pur->vendor;
            $data[]= $pur->progresss;
        }
        //dd($data);
        return view ('magang.dashboardADMIN',['purchase'=> $purchase,'categories'=>$categories,'data'=>$data,]);
    }

    public function dashboardMateri(Request $request)
    {
        if ($request->has('cari')) {
            $purchase = NewPurchase::where('vendor','LIKE','%'. $request->cari. '%')->orWhere('transaction_no','LIKE','%'.$request->cari."%")->orWhere('due_date','LIKE','%'.$request->cari."%")->orWhere('progresss','LIKE','%'.$request->cari."%")->orWhere('descis','LIKE','%'.$request->cari."%")->get();
        } else {
            $purchase = NewPurchase::all();
        }
        return view ('magang.materialis',compact('purchase'));
    }


    public function detailPurchase($id)
    {
        $purchase = NewPurchase::with('detail')->where('id', $id)->first();
        //dd($purchase);
        return view ('magang.detailpurchase',compact('purchase'));
    }

    // public function dashboardCustomer($id)
    // {
    //     $purchase = User::with('project')-> whereColumn('id', $id)->first();
    //     $purchases = NewPurchase::where('vendor',$purchase->company)->first();
    //     //$purchase = User::with('project')-> whereColumn('company', 'vendor')->first();
    //     return view ('magang.dashboardCUSTOMER',compact('purchase'));
    // }

    public function deletePurchase($id)
    {
        // DB::table('new_purchases')->where('id',$id)->delete();
        // DB::table('new_purchases')->where('id',$id)->delete();

        $purchase = NewPurchase::with('detail','materialist','materialistpro')->where('id', $id)->first();
        $purchase->delete();
        $purchases = NewMaterialist::where('order_id',$purchase->id)->delete();
        //$purchases->delete();
        $purchaseses = NewMaterialistProduct::where('materi_id',$purchase->id)->delete();
        //$purchaseses->delete();
        $purchaseseses = NewPurchaseProduct::where('user_id',$purchase->id)->delete();
        //$purchaseseses->delete();
        //dd($purchase);
        return redirect()->route('detail.purchase')->with('sukses','Data berhasil dihapus');
    }

    public function detailMaterialist($id)
    {
        $purchase = NewPurchase::with('materialist','materialistpro')->where('id', $id)->first();
        //dd($purchase->materialistpro);
        return view ('magang.detailmaterialist',compact('purchase'));
    }

    public function printMaterialist($id)
    {
        $purchase = NewPurchase::with('materialist','materialistpro')->where('id', $id)->first();
        //dd($purchase->materialistpro);
        return view ('magang.printmaterialist',compact('purchase'));
    }

    public function deleteMaterialist($id)
    {
        $purchase = NewPurchase::with('materialist','materialistpro')->where('id', $id)->first();
        $purchases = NewMaterialist::where('order_id',$purchase->id)->first();
        $purchases->update(['tower_type' => null]);
        $purchases->update(['pack_list_no' => null]);
        $purchases->update(['num_rev' => null]);
        $purchases->update(['total_done' => null]);
        $purchases->update(['progress' => null]);
        $purchases->update(['acknowledged' => null]);
        $purchases->update(['recipient' => null]);
        $purchases->update(['warehouse' => null]);
        $purchases->update(['total_qty' => null]);
        $purchases->update(['desci' => null]);

        $purchase->update(['progresss' => null]);
        $purchase->update(['descis' => null]);
        // $purchases->tower_type = $request->delete();
        // $purchases->pack_list_no = $request->delete();
        // $purchases->num_rev =  $request->delete();
        // $purchases->total_done =  $request->delete();
        // $purchases->progress =  $request->delete();
        // $purchases->acknowledged =  $request->delete();
        // $purchases->recipient = $request->delete();
        // $purchases->warehouse = $request->delete();
        //$purchases->delete();
        $purchaseses = NewMaterialistProduct::where('materi_id',$purchase->id)->delete();
        //$materi->order_id =  $purchase->id;
        //$purchaseses->delete();
        //dd($purchase->materialistpro);
        return view ('magang.detailmaterialist',compact('purchase'));
    }

    public function edit($id)
    {
        $purchase = NewPurchase::with('detail')->where('id', $id)->first();
        //dd($purchase->detail);
        return view ('magang.editpurchase',compact('purchase'));
    }
    public function editMaterialist($id)
    {
        $purchase = NewPurchase::with('materialist','materialistpro')->where('id', $id)->first();
        //dd($purchase->detail);
        return view ('magang.editmaterialist',compact('purchase'));
    }

   

    
    public function editCustomer($id)
    {
        //dd('qw');
        $user = User::where('id', $id)->first();
        $purchase = NewPurchase::all();
        return view ('magang.editcustomer',compact(['purchase','user']));
    }

    public function addProject(Request $request, $id)
    {
        $purchases = NewPurchase::whereIn('id',$request->purchase)->get();
        //dd($purchases);
        foreach ($purchases as $purchase){
            $purchase->vendor_id=$id;
            $purchase->save();
        }
        $user = User::find($id);
        if ($user->status==0){
            $user->status=1;
            $user->save();
        }
        return redirect('/approval');
    }

    public function editProject($id)
    {
        //dd('qw');
        $user = User::with('project')->where('id', $id)->first();
        //$purchase = NewPurchase::with('project')->where('id', $id)->first();
        return view ('magang.editproject',compact(['user']));
    }

    public function editUser(Request $request, $id)
    {
        $data = $request->all();
        $users = User::where('id', $id)->first();
        $users->role = $request->input('role');
        $users->save();
        return redirect('/approval');
        //dd($purchases);
        
    }

    public function deleteProject($id)
    {
        $user = User::with('project')->where('id', $id)->first();
        $purchase = NewPurchase::where('vendor_id',$user->id)->first();
        $purchase->update(['vendor_id' => null]);
        return redirect()->route('editProject',['id'=>$user->id]);
    }

    public function updateMaterialist(Request $request, $id)
    {
        //dd( $request->all());
        $data = $request->all();
        $purchase = NewPurchase::with('materialist','materialistpro')->where('id', $id)->first();
     
        $purchases = NewMaterialist::where('order_id',$purchase->id)->first();
        $purchases->tower_type = $request->input('tower_type');
        $purchases->pack_list_no = $request->input('pack_list_no');
        $purchases->num_rev =  $request->input('num_rev');
        $purchases->total_done =  $request->input('total_done');
        $purchases->progress =  $request->input('progress');
        $purchases->acknowledged =  $request->input('acknowledged');
        $purchases->recipient = $request->input('recipient');
        $purchases->warehouse = $request->input('warehouse');
        $purchases->desci = $request->input('desci');
        //dd($purchase);
        $purchases->save();

        $purchase_products= NewMaterialistProduct::where('materi_id',$purchase->id)->get();
        $f=0;
        foreach ($purchase_products as $purchase_product){
            // $purchase_product->lastdone= $request->input('lastdone')[$f];
            $purchase_product->done += $request->input('done')[$f];
            $f++;
            $purchase_product->save();

        }
        $purchase->progresss =  $purchases->progress;
        $purchase->descis =  $purchases->desci;
        $purchase->save();
       
        
     
        //$purchase->update($request->all());
        
        //return redirect() ->back()->with('status', 'Data Berhasil di Edit');
        return redirect()->route('detailM',['id'=>$purchase->id]);
    }

    public function materialimport(Request $request){
        $file = $request->file('file');
        $namaFile = $file->getClientOriginalName();
        $file->move('DataMaterial', $namaFile);

        \Excel::import(new MaterialImport($request->id), public_path('/DataMaterial/'.$namaFile));
        $materialis=NewMaterialist::where('order_id',$request->id)->first();
        $materialis_products= NewMaterialistProduct::where('materi_id',$request->id)->get();

        $tot_qty=$materialis_products->sum('tot_qty');
        $materialis->total_qty= $tot_qty;
        $materialis->save();
        //dd($file);
        return redirect() ->back()->with('status', 'Data Berhasil di Input');
    }

    public function update(Request $request, $id)
    {
        // $data = $request->all();
        // // dd($data);
        // $nm = $data['attachments'];
        // $namaFile = $nm->getClientOriginalName();

        // $purchase = new  NewPurchase;
        // $purchase->vendor = $data['vendor'];
        // $purchase->email= $data['email'];
        // $purchase->vendor_adress = $data['vendor_adress'];
        // $purchase->transaction_date = $data['transaction_date'];
        // $purchase->due_date = $data['due_date'];
        // $purchase->transaction_no = $data['transaction_no'];
        // $purchase->vendor_ref_no = $data['vendor_ref_no'];
        // $purchase->term = $data['term'];
        // $purchase->subtotal = $data['subtotal'];
        // $purchase->tax = $data['tax'];
        // $purchase->totalakhir = $data['totalakhir'];
        // $purchase->message = $data['message'];
        // $purchase->note = $data['note'];
        // // $purchase->attachments = $data['attachments'];
        // $purchase->attachments = $namaFile;
        // $nm->move(public_path().'/pdf',$namaFile);
        // $purchase->save();

        
        // foreach ($data['select_product'] as $key => $select_product) {
        //    NewPurchaseProduct::create(
        //         [
        //             'user_id'=> $purchase->id, 
        //             'select_product' => $data['select_product'][$key],
        //             'desc' => $data['desc'][$key],
        //             'qty' => $data['qty'][$key],
        //             'units' => $data['units'][$key],
        //             'units_price' => $data['units_price'][$key],
        //             'subtax' => $data['subtax'][$key],
        //             'amount' => $data['amount'][$key]

        //         ]
        //     );
        // }
        
        $data = $request->all();
        $purchase = NewPurchase::with('detail')->where('id', $id)->first();
        $purchase->vendor = $request->input('vendor');
        $purchase->email = $request->input('email');
        $purchase->vendor_adress = $request->input('vendor_adress');
        $purchase->transaction_date = $request->input('transaction_date');
        $purchase->due_date = $request->input('due_date');
        $purchase->transaction_no = $request->input('transaction_no');
        $purchase->vendor_ref_no = $request->input('vendor_ref_no');
        $purchase->term = $request->input('term');
        $purchase->subtotal = $request->input('subtotal');
        $purchase->tax = $request->input('tax');
        $purchase->totalakhir = $request->input('totalakhir');
        $purchase->message = $request->input('message');
        $purchase->note = $request->input('note');
        $purchase->tax = $request->input('tax');

        if ($request->hasfile('attachments')){
            $file = $request->file('attachments');
            $extension = $file->getClientOriginalExtension();
            $filename = time(). '.'. $extension;
            $file->move(public_path().'/pdf', $filename);
            $purchase->attachments= $filename;
        }
        $purchase->save();
        $purchase_products= NewPurchaseProduct::where('user_id',$purchase->id)->get();
        $i=0;
        foreach ($purchase_products as $purchase_product){
            //dd($purchase_product);
            $purchase_product->select_product= $request->input('select_product')[$i];
            $purchase_product->desc= $request->input('desc')[$i];
            $purchase_product->qty= $request->input('qty')[$i];
            $purchase_product->units= $request->input('units')[$i];
            $purchase_product->units_price= $request->input('units_price')[$i];
            $purchase_product->subtax= $request->input('subtax')[$i];
            $purchase_product->amount= $request->input('amount')[$i];
            $i++;
            $purchase_product->delete();

        }
        
        foreach ($data['select_product'] as $key => $select_product) {
            NewPurchaseProduct::create(
                 [
                     'user_id'=> $purchase->id, 
                     'select_product' => $data['select_product'][$key],
                     'desc' => $data['desc'][$key],
                     'qty' => $data['qty'][$key],
                     'units' => $data['units'][$key],
                     'units_price' => $data['units_price'][$key],
                     'subtax' => $data['subtax'][$key],
                     'amount' => $data['amount'][$key]
 
                 ]
             );
         }
        //$purchase->update($request->all());
        //dd($purchase);
        //return redirect() ->back()->with('status', 'Data Berhasil di Edit');
        return redirect()->route('detail',['id'=>$purchase->id]);
    }

    public function delete($id)
    {
        $item= NewPurchaseProduct::find($id);
        $item->delete();
        // DB::table('new_purchase_product')->where('id',$id)-delete();
        return redirect() ->back();
        // return "delete";
    }

    // protected function create(array $data)
    // {
    //     return User::create([
    //         'username'=> $data['username'],
    //         'company'=> $data ['company'],
    //         'email'=> $data ['email'],
    //         'password' => Hash::make($data['password']),
    //         'role' => $data['role'],
    //         'status' => false,
    //     ]);
    // }



    public function regis()
    {
        $users = DB:: table('users')->get();
        return view('magang.approval', ['users'=>$users]);
    }

    public function getLogin()
    {
        return view('magang.login');
    }

    public function postLogin(Request $request)
    {
        // $this->validate($request, [
        //     'email'=>'required',
        //     'password'=>'required'
        // ]);
       
        if(\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Super Admin' ,'status'=>$request->status=1])){
            //session(['berhasil_login'=>true]);
            return redirect('/approval');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Admin', 'status'=>$request->status=1])){
            //session(['berhasil_login'=>true]);
            return redirect('/dashboardADMIN');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Customer', 'status'=>$request->status=1])){
            //session(['berhasil_login'=>true]);
            return redirect('/dashboardCUSTOMER');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Manager', 'status'=>$request->status=1])){
            //session(['berhasil_login'=>true]);
            return redirect('/dashboardManager');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Super Admin', 'status'=>$request->status=0])){
            return redirect('/login')->with('message','Account is Not Active');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Admin', 'status'=>$request->status=0])){
            return redirect('/login')->with('message','Account is Not Active');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Manager', 'status'=>$request->status=0])){
            return redirect('/login')->with('message','Account is Not Active');}
        else if (\Auth::attempt(['username'=>$request->username,'password'=>$request->password,'role'=>$request->role='Customer', 'status'=>$request->status=0])){
            return redirect('/login')->with('message','Account is Not Active');}
        else{
                return redirect('/login')->with('message','Invalid Email or Password');
            }       
            
    }

    public function getSignup()
    {
        return view('magang.register');
    }

    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'username'=>'required|min:4|unique:users',
            'company'=>'required|min:2',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed',
            'role'=>'required',
        ]);

       $user = User::create([
        //    'name'=> $request->name,
        //    'email'=> $request->email,
        //    'password'=> bcrypt($request->password)
            'username'=> $request['username'],
            'company'=> $request ['company'],
            'email'=> $request ['email'],
            'password' => Hash::make($request['password']),
            'role' => $request['role'],
            'status' => false,
       ]);

       $admins = User::where('role', 'Super Admin')->get();

       foreach ($admins as $admin) {
           Mail::to($admin)->send(new UserActivation($user, $admin));
       }

       return redirect('/login');
    }

    public function status(Request $request, $id){
        $data = User::find($id);

        if ($data->status == 0) {
            $data->status=1;
        }else{
            $data->status=0;
        }
        $data->save();

        return redirect('/approval');
    }

    public function logout (Request $request){
        Auth::logout();
        return redirect('/login');
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(),'password');
        $credentials['status']= '1';
        return $credentials;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans(auth.failed)];

        $user = User::where($this->username(), $request->{$this->username()})->first();

        if($user && Hash::check($request->password, $user->password) && $user->status !=1) {
            $errors = [$this->username()=> 'Your account is not activated.'];
        }

        if($request->expectsJson()) {
            return response()->json($errors,422);
        }

        return redirect()->back()->withInput($request->only($this->username(), 'remember'))->withErrors($errors);
    }

    public function dashboardCustomer()
    {
        $purchase= User::with('project')->where('id', auth()->user()->id)->first();
        $categories = [];
        $data = [];
        foreach ($purchase->project as $pur){
            $categories[] = $pur->id;
            $data[]= $pur->progresss;
        }
        //dd($data);
        return view ('magang.dashboardCUSTOMER',['purchase'=> $purchase,'categories'=>$categories,'data'=>$data,]);
    }

    public function dashboardManager()
    {
        $purchase = NewPurchase::all();
        //Menyiapkan data untuk chart
        $categories = [];
        $data = [];
        foreach ($purchase as $pur){
            $categories[] = $pur->vendor;
            $data[]= $pur->progresss;
        }
        //dd($data);
        return view ('magang.dashboardManager',['purchase'=> $purchase,'categories'=>$categories,'data'=>$data,]);
    }

    public function userActivation(Request $request, User $id) 
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $admin = User::select('password')->where('username', $request->username)->first();

        if($admin->password == $request->password) {
            $id->status = 1;
            $id->save();
            return redirect('/');
        }
    }


    // public function tambah (Request $request)
    // {
    //     $data = $request->all();
    //     // dd($data);
    //     $nm = $data['attachments'];
    //     $namaFile = $nm->getClientOriginalName();

    //     $purchase = new  NewPurchase;
    //     $purchase->vendor = $data['vendor'];
    //     $purchase->email= $data['email'];
    //     $purchase->vendor_adress = $data['vendor_adress'];
    //     $purchase->transaction_date = $data['transaction_date'];
    //     $purchase->due_date = $data['due_date'];
    //     $purchase->transaction_no = $data['transaction_no'];
    //     $purchase->vendor_ref_no = $data['vendor_ref_no'];
    //     $purchase->term = $data['term'];
    //     $purchase->subtotal = $data['subtotal'];
    //     $purchase->tax = $data['tax'];
    //     $purchase->totalakhir = $data['totalakhir'];
    //     $purchase->message = $data['message'];
    //     $purchase->note = $data['note'];
    //     // $purchase->attachments = $data['attachments'];
    //     $purchase->attachments = $namaFile;
    //     $nm->move(public_path().'/pdf',$namaFile);
    //     $purchase->save();

        
    //     foreach ($data['select_product'] as $key => $select_product) {
    //        NewPurchaseProduct::create(
    //             [
    //                 'user_id'=> $purchase->id, 
    //                 'select_product' => $data['select_product'][$key],
    //                 'desc' => $data['desc'][$key],
    //                 'qty' => $data['qty'][$key],
    //                 'units' => $data['units'][$key],
    //                 'units_price' => $data['units_price'][$key],
    //                 'subtax' => $data['subtax'][$key],
    //                 'amount' => $data['amount'][$key]

    //             ]
    //         );
    //     }

    //     return redirect()->route('detail',['id'=>$purchase->id]);
    // }
}
