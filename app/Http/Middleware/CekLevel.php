<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CekLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$levels)
    {
    if (auth()->check()) {
        if (in_array(auth()->user()->role, $levels)) {
            return $next($request);
        }
    }
        return redirect('/');
    }
}
